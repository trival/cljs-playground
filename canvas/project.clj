(defproject canvas "0.1.0-SNAPSHOT"
  :plugins [[lein-cljsbuild "0.3.3"]]
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/clojurescript "0.0-1913"]
                 [org.clojure/core.async "0.1.242.0-44b1e3-alpha"]
                 [com.cemerick/piggieback "0.1.0"]]
  :jvm-opts ["-Xms2g" "-Xmx2g"]
  :cljsbuild {
    :builds [
      { :source-paths ["src"]
        :compiler {
          :output-to "resources/public/main-dev.js"  ; default: target/cljsbuild-main.js
          :optimizations :whitespace
          :pretty-print true}}

      { :source-paths ["src"]
        :compiler {
          :output-to "resources/public/main-packed.js"  ; default: target/cljsbuild-main.js
          :optimizations :advanced
          :static-fns true
          :pretty-print true}}]})
