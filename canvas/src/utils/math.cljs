(ns utils.math)


(defn rand-n [max-v]
  (Math/floor (* (Math/random) max-v)))


(defn clamp [val min-v max-v]
  (max min-v(min max-v val)))
