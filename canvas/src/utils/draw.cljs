(ns utils.draw
  (:require [utils.math :refer [rand-n]]))


(defn darken [[r g b a] amount]
  (conj (vec (map #(max 0 (- % amount)) [r g b])) a))



(defn rand-pos [max-w max-h]
  [(rand-n max-w) (rand-n max-h)])


(defn rand-color []
  [(rand-n 255) (rand-n 255) (rand-n 255) (Math/random)])


(defn color-to-str[[r g b a]]
  (str "rgba(" r "," g "," b "," a ")"))


(defn style [ctx fill-c stroke-c line-w]
    (set! (.-fillStyle ctx) (color-to-str fill-c))
    (set! (.-strokeStyle ctx) (color-to-str stroke-c))
    (set! (.-lineWidth ctx) line-w))


(defn draw-circle [ctx [x y] rad]
  (doto ctx
    (.beginPath)
    (.arc x y rad 0 (* 2 Math/PI))
    (.fill)
    (.stroke)))
