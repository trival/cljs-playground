(ns utils.vecmath)

(defn vec2+ [[a b][x y]]
  [(+ a x) (+ b y)])

(defn vec2* [[a b] x]
  [(* a x) (* b x)])
