(ns particles.main
  (:require [cljs.core.async :refer [chan <! >! close! take!]]
            [utils.vecmath :refer [vec2+ vec2*]]
            [utils.draw :refer [draw-circle style color-to-str]]
            [utils.math :refer [clamp]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))


(defn particle [pos vel lifetime acc-fn & props]
  (let [c (chan)
        birth (Date/now)
        death (+ birth lifetime)]
    (go-loop [p pos
              v vel
              last (Date/now)]
      (let [now (Date/now)
            life (- 1 (/ (- now birth) lifetime))
            time (/ (- now last) 1000)
            new-v (vec2+ v (vec2* (acc-fn pos now) time))
            tmp-v (vec2* new-v time)
            new-p (vec2+ p tmp-v)]
        (if (>= now death)
          (close! c)
          (do
            (>! c (into {:pos new-p
                         :life life}
                        props))
            (recur new-p new-v now)))))
    c))


#_(let [p (particle [100 100] [50 0] 10000 (fn [_ _] [0 2]))]
  (take! p (fn read [data]
             data
             (js/setTimeout #(take! p read) 100))))


(defn rand-vel [amount]
  [(* (- (Math/random) 0.5) 2 amount) (* (- (Math/random) 0.5) 2 amount)])

#_(rand-vel 50)


(defn lifetime [min-l max-l]
  (max min-l (Math/floor (* (Math/random) max-l))))

#_(lifetime 100 1000)


(defn particle-system [pos acc-fn & {:keys [start-n spawn? n-per-sec vel life-range]}]
  (let [c (chan)
        n (or start-n 20)
        vel (or vel 100)
        lr (or life-range [1000 10000])
        particles (into [] (repeatedly n #(particle pos (rand-vel vel) (apply lifetime lr) acc-fn)))]
    (go-loop [ps particles]
      (let [count-p (count particles)
            now (Date/now)
            data (loop [data {:channels #{} :data #{}}
                        i 0]
                   (if (< i count-p)
                     (let [c (particles i)
                           p (<! c)
                           ret (if p
                                 {:channels (conj (:channels data) c)
                                  :data (conj (:data data) p)}
                                 data)]
                       (recur ret (inc i)))
                     data))]
        (>! c (:data data))
        (recur (:channels data))))
    c))

#_(let [p (particle-system [100 100] (fn [_ _] [0 2]))]
  (take! p (fn [data]
             data
             (js/setTimeout #(take! p (fn [data] data)) 2000))))


(defn draw-particles [ctx w h ps]
  (doseq [p ps]
    (let [l (:life p)
          color [255 255 255 l]
          [x y] (:pos p)
          x (Math/floor (clamp x 0 w))
          y (Math/floor (clamp y 0 h))
          rad (* 10 l)]
      (style ctx color color 1)
      (draw-circle ctx [x y] rad))))


(defn get-ctx []
  (let [c (.getElementById js/document "my-canvas")
        ctx (.getContext c "2d")
        w (.-width c)
        h (.-height c)]
    [ctx w h]))


(defn render []
  (let [[ctx w h] (get-ctx)
        particles (particle-system [200 100] (fn [_ _][0 10]) :start-n 200 :vel 50)
        render-fn (fn render-fn []
                    (take! particles (fn [ps]
                      (.clearRect ctx 0 0 w h)
                      (draw-particles ctx w h ps)
                      (js/requestAnimationFrame render-fn))))]
    (render-fn)))


(render)


#_(let [[ctx w h] (get-ctx)]
  (.clearRect ctx 0 0 w h)
  (draw-particles ctx w h [{:pos [20 40] :life 0.4}
                           {:pos [320 140] :life 0.6}
                           {:pos [20 240] :life 0.2}
                           {:pos [320 340] :life 0.9}
                           {:pos [20 440] :life 0.4}]))
