(ns canvas.test
  (:require [utils.draw :refer [draw-circle rand-pos darken style rand-color]]
            [utils.math :refer [rand-n]]))


(defn main []
  (let [c (.getElementById js/document "my-canvas")
        ctx (.getContext c "2d")
        w (.-width c)
        h (.-height c)]
    (dotimes [n 30]
      (let [color (rand-color)
            darker (darken color 30)
            rad (rand-n 20)
            pos (rand-pos w h)]
        (style ctx color darker 5)
        (draw-circle ctx pos rad)))))



(main)
