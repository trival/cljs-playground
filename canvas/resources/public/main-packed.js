function aa() {
  return function(a) {
    return a
  }
}
function g(a) {
  return function() {
    return this[a]
  }
}
function p(a) {
  return function() {
    return a
  }
}
var r, ba = this;
function u(a) {
  var b = typeof a;
  if("object" == b) {
    if(a) {
      if(a instanceof Array) {
        return"array"
      }
      if(a instanceof Object) {
        return b
      }
      var c = Object.prototype.toString.call(a);
      if("[object Window]" == c) {
        return"object"
      }
      if("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice")) {
        return"array"
      }
      if("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("call")) {
        return"function"
      }
    }else {
      return"null"
    }
  }else {
    if("function" == b && "undefined" == typeof a.call) {
      return"object"
    }
  }
  return b
}
function ca(a) {
  var b = u(a);
  return"array" == b || "object" == b && "number" == typeof a.length
}
function da(a) {
  return"string" == typeof a
}
function ea(a) {
  var b = typeof a;
  return"object" == b && null != a || "function" == b
}
var fa = "closure_uid_" + (1E9 * Math.random() >>> 0), ga = 0;
function ha(a, b, c) {
  return a.call.apply(a.bind, arguments)
}
function ia(a, b, c) {
  if(!a) {
    throw Error();
  }
  if(2 < arguments.length) {
    var d = Array.prototype.slice.call(arguments, 2);
    return function() {
      var c = Array.prototype.slice.call(arguments);
      Array.prototype.unshift.apply(c, d);
      return a.apply(b, c)
    }
  }
  return function() {
    return a.apply(b, arguments)
  }
}
function ja(a, b, c) {
  ja = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? ha : ia;
  return ja.apply(null, arguments)
}
var ka = Date.now || function() {
  return+new Date
};
function la(a, b) {
  function c() {
  }
  c.prototype = b.prototype;
  a.Qc = b.prototype;
  a.prototype = new c;
  a.prototype.constructor = a
}
;function na(a, b) {
  for(var c = 1;c < arguments.length;c++) {
    var d = String(arguments[c]).replace(/\$/g, "$$$$");
    a = a.replace(/\%s/, d)
  }
  return a
}
function oa(a) {
  if(!pa.test(a)) {
    return a
  }
  -1 != a.indexOf("\x26") && (a = a.replace(qa, "\x26amp;"));
  -1 != a.indexOf("\x3c") && (a = a.replace(ra, "\x26lt;"));
  -1 != a.indexOf("\x3e") && (a = a.replace(sa, "\x26gt;"));
  -1 != a.indexOf('"') && (a = a.replace(ta, "\x26quot;"));
  return a
}
var qa = /&/g, ra = /</g, sa = />/g, ta = /\"/g, pa = /[&<>\"]/;
function ua(a) {
  for(var b = 0, c = 0;c < a.length;++c) {
    b = 31 * b + a.charCodeAt(c), b %= 4294967296
  }
  return b
}
;function va(a) {
  Error.captureStackTrace ? Error.captureStackTrace(this, va) : this.stack = Error().stack || "";
  a && (this.message = String(a))
}
la(va, Error);
va.prototype.name = "CustomError";
function wa(a, b) {
  b.unshift(a);
  va.call(this, na.apply(null, b));
  b.shift();
  this.Lc = a
}
la(wa, va);
wa.prototype.name = "AssertionError";
function xa(a, b) {
  throw new wa("Failure" + (a ? ": " + a : ""), Array.prototype.slice.call(arguments, 1));
}
;var ya = Array.prototype, za = ya.indexOf ? function(a, b, c) {
  return ya.indexOf.call(a, b, c)
} : function(a, b, c) {
  c = null == c ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
  if(da(a)) {
    return da(b) && 1 == b.length ? a.indexOf(b, c) : -1
  }
  for(;c < a.length;c++) {
    if(c in a && a[c] === b) {
      return c
    }
  }
  return-1
}, Aa = ya.forEach ? function(a, b, c) {
  ya.forEach.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, e = da(a) ? a.split("") : a, f = 0;f < d;f++) {
    f in e && b.call(c, e[f], f, a)
  }
}, Ba = ya.filter ? function(a, b, c) {
  return ya.filter.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, e = [], f = 0, h = da(a) ? a.split("") : a, k = 0;k < d;k++) {
    if(k in h) {
      var l = h[k];
      b.call(c, l, k, a) && (e[f++] = l)
    }
  }
  return e
};
function Ca(a) {
  var b = a.length;
  if(0 < b) {
    for(var c = Array(b), d = 0;d < b;d++) {
      c[d] = a[d]
    }
    return c
  }
  return[]
}
;function Da(a, b) {
  for(var c in a) {
    b.call(void 0, a[c], c, a)
  }
}
;function Ea(a, b) {
  null != a && this.append.apply(this, arguments)
}
Ea.prototype.za = "";
Ea.prototype.append = function(a, b, c) {
  this.za += a;
  if(null != b) {
    for(var d = 1;d < arguments.length;d++) {
      this.za += arguments[d]
    }
  }
  return this
};
Ea.prototype.toString = g("za");
var Fa;
function v(a) {
  return null != a && !1 !== a
}
function w(a, b) {
  return a[u(null == b ? null : b)] ? !0 : a._ ? !0 : x ? !1 : null
}
function Ga(a) {
  return null == a ? null : a.constructor
}
function y(a, b) {
  var c = Ga(b), c = v(v(c) ? c.Da : c) ? c.Ca : u(b);
  return Error(["No protocol method ", a, " defined for type ", c, ": ", b].join(""))
}
function Ha(a) {
  var b = a.Ca;
  return v(b) ? b : "" + z(a)
}
function Ia(a) {
  return Array.prototype.slice.call(arguments)
}
var Ja = {}, Ka = {};
function La(a) {
  if(a ? a.I : a) {
    return a.I(a)
  }
  var b;
  b = La[u(null == a ? null : a)];
  if(!b && (b = La._, !b)) {
    throw y("ICounted.-count", a);
  }
  return b.call(null, a)
}
function Ma(a) {
  if(a ? a.J : a) {
    return a.J(a)
  }
  var b;
  b = Ma[u(null == a ? null : a)];
  if(!b && (b = Ma._, !b)) {
    throw y("IEmptyableCollection.-empty", a);
  }
  return b.call(null, a)
}
var Na = {};
function Oa(a, b) {
  if(a ? a.G : a) {
    return a.G(a, b)
  }
  var c;
  c = Oa[u(null == a ? null : a)];
  if(!c && (c = Oa._, !c)) {
    throw y("ICollection.-conj", a);
  }
  return c.call(null, a, b)
}
var Pa = {}, A = function() {
  function a(a, b, c) {
    if(a ? a.P : a) {
      return a.P(a, b, c)
    }
    var h;
    h = A[u(null == a ? null : a)];
    if(!h && (h = A._, !h)) {
      throw y("IIndexed.-nth", a);
    }
    return h.call(null, a, b, c)
  }
  function b(a, b) {
    if(a ? a.K : a) {
      return a.K(a, b)
    }
    var c;
    c = A[u(null == a ? null : a)];
    if(!c && (c = A._, !c)) {
      throw y("IIndexed.-nth", a);
    }
    return c.call(null, a, b)
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}(), Qa = {};
function B(a) {
  if(a ? a.N : a) {
    return a.N(a)
  }
  var b;
  b = B[u(null == a ? null : a)];
  if(!b && (b = B._, !b)) {
    throw y("ISeq.-first", a);
  }
  return b.call(null, a)
}
function C(a) {
  if(a ? a.V : a) {
    return a.V(a)
  }
  var b;
  b = C[u(null == a ? null : a)];
  if(!b && (b = C._, !b)) {
    throw y("ISeq.-rest", a);
  }
  return b.call(null, a)
}
var Ra = {}, Sa = {}, Ta = function() {
  function a(a, b, c) {
    if(a ? a.D : a) {
      return a.D(a, b, c)
    }
    var h;
    h = Ta[u(null == a ? null : a)];
    if(!h && (h = Ta._, !h)) {
      throw y("ILookup.-lookup", a);
    }
    return h.call(null, a, b, c)
  }
  function b(a, b) {
    if(a ? a.Q : a) {
      return a.Q(a, b)
    }
    var c;
    c = Ta[u(null == a ? null : a)];
    if(!c && (c = Ta._, !c)) {
      throw y("ILookup.-lookup", a);
    }
    return c.call(null, a, b)
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}();
function Ua(a, b) {
  if(a ? a.Ra : a) {
    return a.Ra(a, b)
  }
  var c;
  c = Ua[u(null == a ? null : a)];
  if(!c && (c = Ua._, !c)) {
    throw y("IAssociative.-contains-key?", a);
  }
  return c.call(null, a, b)
}
function Va(a, b, c) {
  if(a ? a.fa : a) {
    return a.fa(a, b, c)
  }
  var d;
  d = Va[u(null == a ? null : a)];
  if(!d && (d = Va._, !d)) {
    throw y("IAssociative.-assoc", a);
  }
  return d.call(null, a, b, c)
}
var Wa = {}, Xa = {};
function Ya(a) {
  if(a ? a.ib : a) {
    return a.ib(a)
  }
  var b;
  b = Ya[u(null == a ? null : a)];
  if(!b && (b = Ya._, !b)) {
    throw y("IMapEntry.-key", a);
  }
  return b.call(null, a)
}
function Za(a) {
  if(a ? a.sb : a) {
    return a.sb(a)
  }
  var b;
  b = Za[u(null == a ? null : a)];
  if(!b && (b = Za._, !b)) {
    throw y("IMapEntry.-val", a);
  }
  return b.call(null, a)
}
var $a = {}, ab = {};
function bb(a, b, c) {
  if(a ? a.jb : a) {
    return a.jb(a, b, c)
  }
  var d;
  d = bb[u(null == a ? null : a)];
  if(!d && (d = bb._, !d)) {
    throw y("IVector.-assoc-n", a);
  }
  return d.call(null, a, b, c)
}
function cb(a) {
  if(a ? a.Ta : a) {
    return a.Ta(a)
  }
  var b;
  b = cb[u(null == a ? null : a)];
  if(!b && (b = cb._, !b)) {
    throw y("IDeref.-deref", a);
  }
  return b.call(null, a)
}
var db = {};
function eb(a) {
  if(a ? a.w : a) {
    return a.w(a)
  }
  var b;
  b = eb[u(null == a ? null : a)];
  if(!b && (b = eb._, !b)) {
    throw y("IMeta.-meta", a);
  }
  return b.call(null, a)
}
var fb = {};
function gb(a, b) {
  if(a ? a.A : a) {
    return a.A(a, b)
  }
  var c;
  c = gb[u(null == a ? null : a)];
  if(!c && (c = gb._, !c)) {
    throw y("IWithMeta.-with-meta", a);
  }
  return c.call(null, a, b)
}
var hb = {}, ib = function() {
  function a(a, b, c) {
    if(a ? a.S : a) {
      return a.S(a, b, c)
    }
    var h;
    h = ib[u(null == a ? null : a)];
    if(!h && (h = ib._, !h)) {
      throw y("IReduce.-reduce", a);
    }
    return h.call(null, a, b, c)
  }
  function b(a, b) {
    if(a ? a.R : a) {
      return a.R(a, b)
    }
    var c;
    c = ib[u(null == a ? null : a)];
    if(!c && (c = ib._, !c)) {
      throw y("IReduce.-reduce", a);
    }
    return c.call(null, a, b)
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}();
function jb(a, b) {
  if(a ? a.u : a) {
    return a.u(a, b)
  }
  var c;
  c = jb[u(null == a ? null : a)];
  if(!c && (c = jb._, !c)) {
    throw y("IEquiv.-equiv", a);
  }
  return c.call(null, a, b)
}
function kb(a) {
  if(a ? a.C : a) {
    return a.C(a)
  }
  var b;
  b = kb[u(null == a ? null : a)];
  if(!b && (b = kb._, !b)) {
    throw y("IHash.-hash", a);
  }
  return b.call(null, a)
}
var lb = {};
function mb(a) {
  if(a ? a.v : a) {
    return a.v(a)
  }
  var b;
  b = mb[u(null == a ? null : a)];
  if(!b && (b = mb._, !b)) {
    throw y("ISeqable.-seq", a);
  }
  return b.call(null, a)
}
var nb = {};
function D(a, b) {
  if(a ? a.ub : a) {
    return a.ub(0, b)
  }
  var c;
  c = D[u(null == a ? null : a)];
  if(!c && (c = D._, !c)) {
    throw y("IWriter.-write", a);
  }
  return c.call(null, a, b)
}
function ob(a) {
  if(a ? a.Ob : a) {
    return null
  }
  var b;
  b = ob[u(null == a ? null : a)];
  if(!b && (b = ob._, !b)) {
    throw y("IWriter.-flush", a);
  }
  return b.call(null, a)
}
var pb = {};
function qb(a, b, c) {
  if(a ? a.t : a) {
    return a.t(a, b, c)
  }
  var d;
  d = qb[u(null == a ? null : a)];
  if(!d && (d = qb._, !d)) {
    throw y("IPrintWithWriter.-pr-writer", a);
  }
  return d.call(null, a, b, c)
}
function rb(a) {
  if(a ? a.Aa : a) {
    return a.Aa(a)
  }
  var b;
  b = rb[u(null == a ? null : a)];
  if(!b && (b = rb._, !b)) {
    throw y("IEditableCollection.-as-transient", a);
  }
  return b.call(null, a)
}
function sb(a, b) {
  if(a ? a.ia : a) {
    return a.ia(a, b)
  }
  var c;
  c = sb[u(null == a ? null : a)];
  if(!c && (c = sb._, !c)) {
    throw y("ITransientCollection.-conj!", a);
  }
  return c.call(null, a, b)
}
function tb(a) {
  if(a ? a.oa : a) {
    return a.oa(a)
  }
  var b;
  b = tb[u(null == a ? null : a)];
  if(!b && (b = tb._, !b)) {
    throw y("ITransientCollection.-persistent!", a);
  }
  return b.call(null, a)
}
function ub(a, b, c) {
  if(a ? a.ua : a) {
    return a.ua(a, b, c)
  }
  var d;
  d = ub[u(null == a ? null : a)];
  if(!d && (d = ub._, !d)) {
    throw y("ITransientAssociative.-assoc!", a);
  }
  return d.call(null, a, b, c)
}
function wb(a) {
  if(a ? a.nb : a) {
    return a.nb()
  }
  var b;
  b = wb[u(null == a ? null : a)];
  if(!b && (b = wb._, !b)) {
    throw y("IChunk.-drop-first", a);
  }
  return b.call(null, a)
}
function xb(a) {
  if(a ? a.Sa : a) {
    return a.Sa(a)
  }
  var b;
  b = xb[u(null == a ? null : a)];
  if(!b && (b = xb._, !b)) {
    throw y("IChunkedSeq.-chunked-first", a);
  }
  return b.call(null, a)
}
function yb(a) {
  if(a ? a.Ia : a) {
    return a.Ia(a)
  }
  var b;
  b = yb[u(null == a ? null : a)];
  if(!b && (b = yb._, !b)) {
    throw y("IChunkedSeq.-chunked-rest", a);
  }
  return b.call(null, a)
}
function zb(a) {
  this.Zb = a;
  this.m = 0;
  this.e = 1073741824
}
zb.prototype.ub = function(a, b) {
  return this.Zb.append(b)
};
zb.prototype.Ob = p(null);
function Ab(a) {
  var b = new Ea, c = new zb(b);
  a.t(a, c, Bb([Cb, !0, Db, !0, Eb, !1, Fb, !1]));
  ob(c);
  return"" + z(b)
}
function F(a, b, c, d, e) {
  this.qa = a;
  this.name = b;
  this.ra = c;
  this.na = d;
  this.Y = e;
  this.e = 2154168321;
  this.m = 4096
}
r = F.prototype;
r.t = function(a, b) {
  return D(b, this.ra)
};
r.C = function(a) {
  var b = this.na;
  return null != b ? b : this.na = a = Gb.a ? Gb.a(G.c ? G.c(a.qa) : G.call(null, a.qa), G.c ? G.c(a.name) : G.call(null, a.name)) : Gb.call(null, G.c ? G.c(a.qa) : G.call(null, a.qa), G.c ? G.c(a.name) : G.call(null, a.name))
};
r.A = function(a, b) {
  return new F(this.qa, this.name, this.ra, this.na, b)
};
r.w = g("Y");
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return Ta.d(c, this, null);
      case 3:
        return Ta.d(c, this, d)
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.u = function(a, b) {
  return b instanceof F ? this.ra === b.ra : !1
};
r.toString = g("ra");
function H(a) {
  if(null == a) {
    return null
  }
  var b;
  b = a ? ((b = a.e & 8388608) ? b : a.Cc) ? !0 : !1 : !1;
  if(b) {
    return a.v(a)
  }
  if(a instanceof Array || "string" === typeof a) {
    return 0 === a.length ? null : new Hb(a, 0)
  }
  if(w(lb, a)) {
    return mb(a)
  }
  if(x) {
    throw Error([z(a), z("is not ISeqable")].join(""));
  }
  return null
}
function I(a) {
  if(null == a) {
    return null
  }
  var b;
  b = a ? ((b = a.e & 64) ? b : a.Ba) ? !0 : !1 : !1;
  if(b) {
    return a.N(a)
  }
  a = H(a);
  return null == a ? null : B(a)
}
function J(a) {
  if(null != a) {
    var b;
    b = a ? ((b = a.e & 64) ? b : a.Ba) ? !0 : !1 : !1;
    if(b) {
      return a.V(a)
    }
    a = H(a);
    return null != a ? C(a) : L
  }
  return L
}
function M(a) {
  if(null == a) {
    a = null
  }else {
    var b;
    b = a ? ((b = a.e & 128) ? b : a.tb) ? !0 : !1 : !1;
    a = b ? a.U(a) : H(J(a))
  }
  return a
}
var Ib = function() {
  function a(a, b) {
    var c = a === b;
    return c ? c : jb(a, b)
  }
  var b = null, c = function() {
    function a(b, d, k) {
      var l = null;
      2 < arguments.length && (l = N(Array.prototype.slice.call(arguments, 2), 0));
      return c.call(this, b, d, l)
    }
    function c(a, d, e) {
      for(;;) {
        if(v(b.a(a, d))) {
          if(M(e)) {
            a = d, d = I(e), e = M(e)
          }else {
            return b.a(d, I(e))
          }
        }else {
          return!1
        }
      }
    }
    a.n = 2;
    a.l = function(a) {
      var b = I(a);
      a = M(a);
      var d = I(a);
      a = J(a);
      return c(b, d, a)
    };
    a.g = c;
    return a
  }(), b = function(b, e, f) {
    switch(arguments.length) {
      case 1:
        return!0;
      case 2:
        return a.call(this, b, e);
      default:
        return c.g(b, e, N(arguments, 2))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  b.n = 2;
  b.l = c.l;
  b.c = p(!0);
  b.a = a;
  b.g = c.g;
  return b
}();
kb["null"] = p(0);
Ra["null"] = !0;
$a["null"] = !0;
Ka["null"] = !0;
La["null"] = p(0);
jb["null"] = function(a, b) {
  return null == b
};
fb["null"] = !0;
gb["null"] = p(null);
db["null"] = !0;
eb["null"] = p(null);
Ma["null"] = p(null);
Wa["null"] = !0;
Date.prototype.u = function(a, b) {
  var c = b instanceof Date;
  return c ? a.toString() === b.toString() : c
};
kb.number = function(a) {
  return Math.floor(a) % 2147483647
};
jb.number = function(a, b) {
  return a === b
};
kb["boolean"] = function(a) {
  return!0 === a ? 1 : 0
};
db["function"] = !0;
eb["function"] = p(null);
Ja["function"] = !0;
kb._ = function(a) {
  return a[fa] || (a[fa] = ++ga)
};
var Jb = function() {
  function a(a, b, c, d) {
    for(var l = La(a);;) {
      if(d < l) {
        c = b.a ? b.a(c, A.a(a, d)) : b.call(null, c, A.a(a, d)), d += 1
      }else {
        return c
      }
    }
  }
  function b(a, b, c) {
    for(var d = La(a), l = 0;;) {
      if(l < d) {
        c = b.a ? b.a(c, A.a(a, l)) : b.call(null, c, A.a(a, l)), l += 1
      }else {
        return c
      }
    }
  }
  function c(a, b) {
    var c = La(a);
    if(0 === c) {
      return b.i ? b.i() : b.call(null)
    }
    for(var d = A.a(a, 0), l = 1;;) {
      if(l < c) {
        d = b.a ? b.a(d, A.a(a, l)) : b.call(null, d, A.a(a, l)), l += 1
      }else {
        return d
      }
    }
  }
  var d = null, d = function(d, f, h, k) {
    switch(arguments.length) {
      case 2:
        return c.call(this, d, f);
      case 3:
        return b.call(this, d, f, h);
      case 4:
        return a.call(this, d, f, h, k)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  d.a = c;
  d.d = b;
  d.p = a;
  return d
}(), Kb = function() {
  function a(a, b, c, d) {
    for(var l = a.length;;) {
      if(d < l) {
        c = b.a ? b.a(c, a[d]) : b.call(null, c, a[d]), d += 1
      }else {
        return c
      }
    }
  }
  function b(a, b, c) {
    for(var d = a.length, l = 0;;) {
      if(l < d) {
        c = b.a ? b.a(c, a[l]) : b.call(null, c, a[l]), l += 1
      }else {
        return c
      }
    }
  }
  function c(a, b) {
    var c = a.length;
    if(0 === a.length) {
      return b.i ? b.i() : b.call(null)
    }
    for(var d = a[0], l = 1;;) {
      if(l < c) {
        d = b.a ? b.a(d, a[l]) : b.call(null, d, a[l]), l += 1
      }else {
        return d
      }
    }
  }
  var d = null, d = function(d, f, h, k) {
    switch(arguments.length) {
      case 2:
        return c.call(this, d, f);
      case 3:
        return b.call(this, d, f, h);
      case 4:
        return a.call(this, d, f, h, k)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  d.a = c;
  d.d = b;
  d.p = a;
  return d
}();
function Lb(a) {
  if(a) {
    var b = a.e & 2;
    a = (b ? b : a.Jb) ? !0 : a.e ? !1 : w(Ka, a)
  }else {
    a = w(Ka, a)
  }
  return a
}
function Mb(a) {
  if(a) {
    var b = a.e & 16;
    a = (b ? b : a.rb) ? !0 : a.e ? !1 : w(Pa, a)
  }else {
    a = w(Pa, a)
  }
  return a
}
function Hb(a, b) {
  this.b = a;
  this.j = b;
  this.m = 0;
  this.e = 166199550
}
r = Hb.prototype;
r.C = function(a) {
  return Nb.c ? Nb.c(a) : Nb.call(null, a)
};
r.U = function() {
  return this.j + 1 < this.b.length ? new Hb(this.b, this.j + 1) : null
};
r.G = function(a, b) {
  return O.a ? O.a(b, a) : O.call(null, b, a)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return Kb.p(this.b, b, this.b[this.j], this.j + 1)
};
r.S = function(a, b, c) {
  return Kb.p(this.b, b, c, this.j)
};
r.v = aa();
r.I = function() {
  return this.b.length - this.j
};
r.N = function() {
  return this.b[this.j]
};
r.V = function() {
  return this.j + 1 < this.b.length ? new Hb(this.b, this.j + 1) : Ob.i ? Ob.i() : Ob.call(null)
};
r.u = function(a, b) {
  return Pb.a ? Pb.a(a, b) : Pb.call(null, a, b)
};
r.K = function(a, b) {
  var c = b + this.j;
  return c < this.b.length ? this.b[c] : null
};
r.P = function(a, b, c) {
  a = b + this.j;
  return a < this.b.length ? this.b[a] : c
};
r.J = function() {
  return L
};
var Qb = function() {
  function a(a, b) {
    return b < a.length ? new Hb(a, b) : null
  }
  function b(a) {
    return c.a(a, 0)
  }
  var c = null, c = function(c, e) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 2:
        return a.call(this, c, e)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.a = a;
  return c
}(), N = function() {
  function a(a, b) {
    return Qb.a(a, b)
  }
  function b(a) {
    return Qb.a(a, 0)
  }
  var c = null, c = function(c, e) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 2:
        return a.call(this, c, e)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.a = a;
  return c
}();
jb._ = function(a, b) {
  return a === b
};
var Rb = function() {
  function a(a, b) {
    return null != a ? Oa(a, b) : Ob.c ? Ob.c(b) : Ob.call(null, b)
  }
  var b = null, c = function() {
    function a(b, d, k) {
      var l = null;
      2 < arguments.length && (l = N(Array.prototype.slice.call(arguments, 2), 0));
      return c.call(this, b, d, l)
    }
    function c(a, d, e) {
      for(;;) {
        if(v(e)) {
          a = b.a(a, d), d = I(e), e = M(e)
        }else {
          return b.a(a, d)
        }
      }
    }
    a.n = 2;
    a.l = function(a) {
      var b = I(a);
      a = M(a);
      var d = I(a);
      a = J(a);
      return c(b, d, a)
    };
    a.g = c;
    return a
  }(), b = function(b, e, f) {
    switch(arguments.length) {
      case 2:
        return a.call(this, b, e);
      default:
        return c.g(b, e, N(arguments, 2))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  b.n = 2;
  b.l = c.l;
  b.a = a;
  b.g = c.g;
  return b
}();
function P(a) {
  if(null != a) {
    var b;
    b = a ? ((b = a.e & 2) ? b : a.Jb) ? !0 : !1 : !1;
    if(b) {
      a = a.I(a)
    }else {
      if(a instanceof Array) {
        a = a.length
      }else {
        if("string" === typeof a) {
          a = a.length
        }else {
          if(w(Ka, a)) {
            a = La(a)
          }else {
            if(x) {
              a: {
                a = H(a);
                for(b = 0;;) {
                  if(Lb(a)) {
                    a = b + La(a);
                    break a
                  }
                  a = M(a);
                  b += 1
                }
                a = void 0
              }
            }else {
              a = null
            }
          }
        }
      }
    }
  }else {
    a = 0
  }
  return a
}
var Sb = function() {
  function a(a, b, c) {
    for(;;) {
      if(null == a) {
        return c
      }
      if(0 === b) {
        return H(a) ? I(a) : c
      }
      if(Mb(a)) {
        return A.d(a, b, c)
      }
      if(H(a)) {
        a = M(a), b -= 1
      }else {
        return x ? c : null
      }
    }
  }
  function b(a, b) {
    for(;;) {
      if(null == a) {
        throw Error("Index out of bounds");
      }
      if(0 === b) {
        if(H(a)) {
          return I(a)
        }
        throw Error("Index out of bounds");
      }
      if(Mb(a)) {
        return A.a(a, b)
      }
      if(H(a)) {
        var c = M(a), h = b - 1;
        a = c;
        b = h
      }else {
        if(x) {
          throw Error("Index out of bounds");
        }
        return null
      }
    }
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}(), R = function() {
  function a(a, b, c) {
    if(null != a) {
      if(function() {
        var b;
        b = a ? ((b = a.e & 16) ? b : a.rb) ? !0 : !1 : !1;
        return b
      }()) {
        return a.P(a, Math.floor(b), c)
      }
      if(a instanceof Array || "string" === typeof a) {
        return b < a.length ? a[b] : c
      }
      if(w(Pa, a)) {
        return A.a(a, b)
      }
      if(x) {
        if(function() {
          var b;
          b = a ? ((b = a.e & 64) ? b : a.Ba) ? !0 : a.e ? !1 : w(Qa, a) : w(Qa, a);
          return b
        }()) {
          return Sb.d(a, Math.floor(b), c)
        }
        throw Error([z("nth not supported on this type "), z(Ha(Ga(a)))].join(""));
      }
      return null
    }
    return c
  }
  function b(a, b) {
    if(null == a) {
      return null
    }
    if(function() {
      var b;
      b = a ? ((b = a.e & 16) ? b : a.rb) ? !0 : !1 : !1;
      return b
    }()) {
      return a.K(a, Math.floor(b))
    }
    if(a instanceof Array || "string" === typeof a) {
      return b < a.length ? a[b] : null
    }
    if(w(Pa, a)) {
      return A.a(a, b)
    }
    if(x) {
      if(function() {
        var b;
        b = a ? ((b = a.e & 64) ? b : a.Ba) ? !0 : a.e ? !1 : w(Qa, a) : w(Qa, a);
        return b
      }()) {
        return Sb.a(a, Math.floor(b))
      }
      throw Error([z("nth not supported on this type "), z(Ha(Ga(a)))].join(""));
    }
    return null
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}(), Tb = function() {
  function a(a, b, c) {
    if(null != a) {
      var h;
      h = a ? ((h = a.e & 256) ? h : a.hb) ? !0 : !1 : !1;
      a = h ? a.D(a, b, c) : a instanceof Array ? b < a.length ? a[b] : c : "string" === typeof a ? b < a.length ? a[b] : c : w(Sa, a) ? Ta.d(a, b, c) : x ? c : null
    }else {
      a = c
    }
    return a
  }
  function b(a, b) {
    var c;
    null == a ? c = null : (c = a ? ((c = a.e & 256) ? c : a.hb) ? !0 : !1 : !1, c = c ? a.Q(a, b) : a instanceof Array ? b < a.length ? a[b] : null : "string" === typeof a ? b < a.length ? a[b] : null : w(Sa, a) ? Ta.a(a, b) : null);
    return c
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}(), Vb = function() {
  function a(a, b, c) {
    return null != a ? Va(a, b, c) : Ub.a ? Ub.a(b, c) : Ub.call(null, b, c)
  }
  var b = null, c = function() {
    function a(b, d, k, l) {
      var m = null;
      3 < arguments.length && (m = N(Array.prototype.slice.call(arguments, 3), 0));
      return c.call(this, b, d, k, m)
    }
    function c(a, d, e, l) {
      for(;;) {
        if(a = b.d(a, d, e), v(l)) {
          d = I(l), e = I(M(l)), l = M(M(l))
        }else {
          return a
        }
      }
    }
    a.n = 3;
    a.l = function(a) {
      var b = I(a);
      a = M(a);
      var d = I(a);
      a = M(a);
      var l = I(a);
      a = J(a);
      return c(b, d, l, a)
    };
    a.g = c;
    return a
  }(), b = function(b, e, f, h) {
    switch(arguments.length) {
      case 3:
        return a.call(this, b, e, f);
      default:
        return c.g(b, e, f, N(arguments, 3))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  b.n = 3;
  b.l = c.l;
  b.d = a;
  b.g = c.g;
  return b
}();
function Xb(a) {
  var b = "function" == u(a);
  return b ? b : a ? v(v(null) ? null : a.Ib) ? !0 : a.Pb ? !1 : w(Ja, a) : w(Ja, a)
}
var $b = function Yb(b, c) {
  return function() {
    var c = Xb(b);
    c && (c = b ? ((c = b.e & 262144) ? c : b.Gc) ? !0 : b.e ? !1 : w(fb, b) : w(fb, b), c = !c);
    return c
  }() ? Yb(function() {
    "undefined" === typeof Fa && (Fa = {}, Fa = function(b, c, f, h) {
      this.h = b;
      this.mb = c;
      this.ac = f;
      this.Tb = h;
      this.m = 0;
      this.e = 393217
    }, Fa.Da = !0, Fa.Ca = "cljs.core/t17316", Fa.Ka = function(b, c) {
      return D(c, "cljs.core/t17316")
    }, Fa.prototype.call = function() {
      function b(d, h) {
        d = this;
        var k = null;
        1 < arguments.length && (k = N(Array.prototype.slice.call(arguments, 1), 0));
        return c.call(this, d, k)
      }
      function c(b, d) {
        return Zb.a ? Zb.a(b.mb, d) : Zb.call(null, b.mb, d)
      }
      b.n = 1;
      b.l = function(b) {
        var d = I(b);
        b = J(b);
        return c(d, b)
      };
      b.g = c;
      return b
    }(), Fa.prototype.apply = function(b, c) {
      b = this;
      return b.call.apply(b, [b].concat(c.slice()))
    }, Fa.prototype.Ib = !0, Fa.prototype.w = g("Tb"), Fa.prototype.A = function(b, c) {
      return new Fa(this.h, this.mb, this.ac, c)
    });
    return new Fa(c, b, Yb, null)
  }(), c) : gb(b, c)
};
function ac(a) {
  var b;
  b = a ? ((b = a.e & 131072) ? b : a.Mb) ? !0 : a.e ? !1 : w(db, a) : w(db, a);
  return b ? eb(a) : null
}
var bc = {}, cc = 0, G = function() {
  function a(a, b) {
    var c = da(a);
    (c ? b : c) ? (255 < cc && (bc = {}, cc = 0), c = bc[a], "number" !== typeof c && (c = ua(a), bc[a] = c, cc += 1)) : c = kb(a);
    return c
  }
  function b(a) {
    return c.a(a, !0)
  }
  var c = null, c = function(c, e) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 2:
        return a.call(this, c, e)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.a = a;
  return c
}();
function dc(a) {
  if(null == a) {
    a = !1
  }else {
    if(a) {
      var b = a.e & 8;
      a = (b ? b : a.xc) ? !0 : a.e ? !1 : w(Na, a)
    }else {
      a = w(Na, a)
    }
  }
  return a
}
function ec(a) {
  if(null == a) {
    a = !1
  }else {
    if(a) {
      var b = a.e & 4096;
      a = (b ? b : a.Ec) ? !0 : a.e ? !1 : w($a, a)
    }else {
      a = w($a, a)
    }
  }
  return a
}
function fc(a) {
  if(a) {
    var b = a.e & 16777216;
    a = (b ? b : a.Dc) ? !0 : a.e ? !1 : w(nb, a)
  }else {
    a = w(nb, a)
  }
  return a
}
function gc(a) {
  if(null == a) {
    a = !1
  }else {
    if(a) {
      var b = a.e & 1024;
      a = (b ? b : a.Ac) ? !0 : a.e ? !1 : w(Wa, a)
    }else {
      a = w(Wa, a)
    }
  }
  return a
}
function hc(a) {
  if(a) {
    var b = a.e & 16384;
    a = (b ? b : a.Fc) ? !0 : a.e ? !1 : w(ab, a)
  }else {
    a = w(ab, a)
  }
  return a
}
function ic(a) {
  if(a) {
    var b = a.m & 512;
    a = (b ? b : a.wc) ? !0 : !1
  }else {
    a = !1
  }
  return a
}
function jc(a) {
  var b = [];
  Da(a, function(a, d) {
    return b.push(d)
  });
  return b
}
function kc(a, b, c, d, e) {
  for(;0 !== e;) {
    c[d] = a[b], d += 1, e -= 1, b += 1
  }
}
var lc = {};
function mc(a) {
  if(null == a) {
    a = !1
  }else {
    if(a) {
      var b = a.e & 64;
      a = (b ? b : a.Ba) ? !0 : a.e ? !1 : w(Qa, a)
    }else {
      a = w(Qa, a)
    }
  }
  return a
}
function nc(a) {
  return v(a) ? !0 : !1
}
function oc(a, b) {
  if(a === b) {
    return 0
  }
  if(null == a) {
    return-1
  }
  if(null == b) {
    return 1
  }
  if(Ga(a) === Ga(b)) {
    var c;
    c = a ? ((c = a.m & 2048) ? c : a.pb) ? !0 : !1 : !1;
    return c ? a.qb(a, b) : a > b ? 1 : a < b ? -1 : 0
  }
  if(x) {
    throw Error("compare on non-nil objects of different types");
  }
  return null
}
var pc = function() {
  function a(a, b, c, h) {
    for(;;) {
      var k = oc(R.a(a, h), R.a(b, h)), l = 0 === k;
      if(l ? h + 1 < c : l) {
        h += 1
      }else {
        return k
      }
    }
  }
  function b(a, b) {
    var f = P(a), h = P(b);
    return f < h ? -1 : f > h ? 1 : x ? c.p(a, b, f, 0) : null
  }
  var c = null, c = function(c, e, f, h) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 4:
        return a.call(this, c, e, f, h)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.p = a;
  return c
}(), S = function() {
  function a(a, b, c) {
    for(c = H(c);;) {
      if(c) {
        b = a.a ? a.a(b, I(c)) : a.call(null, b, I(c)), c = M(c)
      }else {
        return b
      }
    }
  }
  function b(a, b) {
    var c = H(b);
    return c ? qc.d ? qc.d(a, I(c), M(c)) : qc.call(null, a, I(c), M(c)) : a.i ? a.i() : a.call(null)
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}(), qc = function() {
  function a(a, b, c) {
    var h;
    h = c ? ((h = c.e & 524288) ? h : c.Nb) ? !0 : !1 : !1;
    return h ? c.S(c, a, b) : c instanceof Array ? Kb.d(c, a, b) : "string" === typeof c ? Kb.d(c, a, b) : w(hb, c) ? ib.d(c, a, b) : x ? S.d(a, b, c) : null
  }
  function b(a, b) {
    var c;
    c = b ? ((c = b.e & 524288) ? c : b.Nb) ? !0 : !1 : !1;
    return c ? b.R(b, a) : b instanceof Array ? Kb.a(b, a) : "string" === typeof b ? Kb.a(b, a) : w(hb, b) ? ib.a(b, a) : x ? S.a(a, b) : null
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}();
function rc(a) {
  return 0 <= (a - a % 2) / 2 ? Math.floor.c ? Math.floor.c((a - a % 2) / 2) : Math.floor.call(null, (a - a % 2) / 2) : Math.ceil.c ? Math.ceil.c((a - a % 2) / 2) : Math.ceil.call(null, (a - a % 2) / 2)
}
function sc(a) {
  a -= a >> 1 & 1431655765;
  a = (a & 858993459) + (a >> 2 & 858993459);
  return 16843009 * (a + (a >> 4) & 252645135) >> 24
}
var z = function() {
  function a(a) {
    return null == a ? "" : a.toString()
  }
  var b = null, c = function() {
    function a(b, d) {
      var k = null;
      1 < arguments.length && (k = N(Array.prototype.slice.call(arguments, 1), 0));
      return c.call(this, b, k)
    }
    function c(a, d) {
      return function(a, c) {
        for(;;) {
          if(v(c)) {
            var d = a.append(b.c(I(c))), e = M(c);
            a = d;
            c = e
          }else {
            return a.toString()
          }
        }
      }.call(null, new Ea(b.c(a)), d)
    }
    a.n = 1;
    a.l = function(a) {
      var b = I(a);
      a = J(a);
      return c(b, a)
    };
    a.g = c;
    return a
  }(), b = function(b, e) {
    switch(arguments.length) {
      case 0:
        return"";
      case 1:
        return a.call(this, b);
      default:
        return c.g(b, N(arguments, 1))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  b.n = 1;
  b.l = c.l;
  b.i = p("");
  b.c = a;
  b.g = c.g;
  return b
}();
function Pb(a, b) {
  return nc(fc(b) ? function() {
    for(var c = H(a), d = H(b);;) {
      if(null == c) {
        return null == d
      }
      if(null == d) {
        return!1
      }
      if(Ib.a(I(c), I(d))) {
        c = M(c), d = M(d)
      }else {
        return x ? !1 : null
      }
    }
  }() : null)
}
function Gb(a, b) {
  return a ^ b + 2654435769 + (a << 6) + (a >> 2)
}
function Nb(a) {
  return qc.d(function(a, c) {
    return Gb(a, G.a(c, !1))
  }, G.a(I(a), !1), M(a))
}
function tc(a) {
  var b = 0;
  for(a = H(a);;) {
    if(a) {
      var c = I(a), b = (b + (G.c(uc.c ? uc.c(c) : uc.call(null, c)) ^ G.c(vc.c ? vc.c(c) : vc.call(null, c)))) % 4503599627370496;
      a = M(a)
    }else {
      return b
    }
  }
}
function wc(a, b, c, d, e) {
  this.h = a;
  this.Ea = b;
  this.ha = c;
  this.count = d;
  this.k = e;
  this.m = 0;
  this.e = 65937646
}
r = wc.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.U = function() {
  return 1 === this.count ? null : this.ha
};
r.G = function(a, b) {
  return new wc(this.h, b, a, this.count + 1, null)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = aa();
r.I = g("count");
r.N = g("Ea");
r.V = function() {
  return 1 === this.count ? L : this.ha
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new wc(b, this.Ea, this.ha, this.count, this.k)
};
r.w = g("h");
r.J = function() {
  return L
};
function xc(a) {
  this.h = a;
  this.m = 0;
  this.e = 65937614
}
r = xc.prototype;
r.C = p(0);
r.U = p(null);
r.G = function(a, b) {
  return new wc(this.h, b, null, 1, null)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = p(null);
r.I = p(0);
r.N = p(null);
r.V = function() {
  return L
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new xc(b)
};
r.w = g("h");
r.J = aa();
var L = new xc(null), Ob = function() {
  function a(a) {
    var d = null;
    0 < arguments.length && (d = N(Array.prototype.slice.call(arguments, 0), 0));
    return b.call(this, d)
  }
  function b(a) {
    var b;
    if(a instanceof Hb) {
      b = a.b
    }else {
      a: {
        for(b = [];;) {
          if(null != a) {
            b.push(a.N(a)), a = a.U(a)
          }else {
            break a
          }
        }
        b = void 0
      }
    }
    a = b.length;
    for(var e = L;;) {
      if(0 < a) {
        var f = a - 1, e = e.G(e, b[a - 1]);
        a = f
      }else {
        return e
      }
    }
  }
  a.n = 0;
  a.l = function(a) {
    a = H(a);
    return b(a)
  };
  a.g = b;
  return a
}();
function yc(a, b, c, d) {
  this.h = a;
  this.Ea = b;
  this.ha = c;
  this.k = d;
  this.m = 0;
  this.e = 65929452
}
r = yc.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.U = function() {
  return null == this.ha ? null : H(this.ha)
};
r.G = function(a, b) {
  return new yc(null, b, a, this.k)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = aa();
r.N = g("Ea");
r.V = function() {
  return null == this.ha ? L : this.ha
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new yc(b, this.Ea, this.ha, this.k)
};
r.w = g("h");
r.J = function() {
  return $b(L, this.h)
};
function O(a, b) {
  var c = null == b;
  c || (c = b ? ((c = b.e & 64) ? c : b.Ba) ? !0 : !1 : !1);
  return c ? new yc(null, a, b, null) : new yc(null, a, H(b), null)
}
kb.string = function(a) {
  return ua(a)
};
function T(a, b, c, d) {
  this.qa = a;
  this.name = b;
  this.la = c;
  this.na = d;
  this.e = 2153775105;
  this.m = 4096
}
r = T.prototype;
r.t = function(a, b) {
  return D(b, [z(":"), z(this.la)].join(""))
};
r.C = function() {
  null == this.na && (this.na = Gb(G.c(this.qa), G.c(this.name)) + 2654435769);
  return this.na
};
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        var e;
        null == c ? e = null : (e = c ? ((e = c.e & 256) ? e : c.hb) ? !0 : c.e ? !1 : w(Sa, c) : w(Sa, c), e = e ? Ta.d(c, this, null) : null);
        return e;
      case 3:
        return null == c ? e = d : (e = c ? ((e = c.e & 256) ? e : c.hb) ? !0 : c.e ? !1 : w(Sa, c) : w(Sa, c), e = e ? Ta.d(c, this, d) : d), e
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.u = function(a, b) {
  return b instanceof T ? this.la === b.la : !1
};
r.toString = function() {
  return[z(":"), z(this.la)].join("")
};
function zc(a, b) {
  var c;
  c = a === b ? !0 : ((c = a instanceof T) ? b instanceof T : c) ? a.la === b.la : !1;
  return c
}
var Bc = function() {
  function a(a, b) {
    return new T(a, b, [z(v(a) ? [z(a), z("/")].join("") : null), z(b)].join(""), null)
  }
  function b(a) {
    return a instanceof T ? a : a instanceof F ? new T(null, Ac.c ? Ac.c(a) : Ac.call(null, a), Ac.c ? Ac.c(a) : Ac.call(null, a), null) : x ? new T(null, a, a, null) : null
  }
  var c = null, c = function(c, e) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 2:
        return a.call(this, c, e)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.a = a;
  return c
}();
function Cc(a, b, c, d) {
  this.h = a;
  this.xa = b;
  this.r = c;
  this.k = d;
  this.m = 0;
  this.e = 32374988
}
r = Cc.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.U = function(a) {
  a.v(a);
  return null == this.r ? null : M(this.r)
};
r.G = function(a, b) {
  return O(b, a)
};
r.toString = function() {
  return Ab(this)
};
function Dc(a) {
  null != a.xa && (a.r = a.xa.i ? a.xa.i() : a.xa.call(null), a.xa = null);
  return a.r
}
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = function(a) {
  Dc(a);
  if(null == this.r) {
    return null
  }
  for(a = this.r;;) {
    if(a instanceof Cc) {
      a = Dc(a)
    }else {
      return this.r = a, H(this.r)
    }
  }
};
r.N = function(a) {
  a.v(a);
  return null == this.r ? null : I(this.r)
};
r.V = function(a) {
  a.v(a);
  return null != this.r ? J(this.r) : L
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new Cc(b, this.xa, this.r, this.k)
};
r.w = g("h");
r.J = function() {
  return $b(L, this.h)
};
function Ec(a, b) {
  this.H = a;
  this.end = b;
  this.m = 0;
  this.e = 2
}
Ec.prototype.I = g("end");
Ec.prototype.add = function(a) {
  this.H[this.end] = a;
  return this.end += 1
};
Ec.prototype.Z = function() {
  var a = new Fc(this.H, 0, this.end);
  this.H = null;
  return a
};
function Fc(a, b, c) {
  this.b = a;
  this.s = b;
  this.end = c;
  this.m = 0;
  this.e = 524306
}
r = Fc.prototype;
r.R = function(a, b) {
  return Kb.p(this.b, b, this.b[this.s], this.s + 1)
};
r.S = function(a, b, c) {
  return Kb.p(this.b, b, c, this.s)
};
r.nb = function() {
  if(this.s === this.end) {
    throw Error("-drop-first of empty chunk");
  }
  return new Fc(this.b, this.s + 1, this.end)
};
r.K = function(a, b) {
  return this.b[this.s + b]
};
r.P = function(a, b, c) {
  return((a = 0 <= b) ? b < this.end - this.s : a) ? this.b[this.s + b] : c
};
r.I = function() {
  return this.end - this.s
};
var Gc = function() {
  function a(a, b, c) {
    return new Fc(a, b, c)
  }
  function b(a, b) {
    return new Fc(a, b, a.length)
  }
  function c(a) {
    return new Fc(a, 0, a.length)
  }
  var d = null, d = function(d, f, h) {
    switch(arguments.length) {
      case 1:
        return c.call(this, d);
      case 2:
        return b.call(this, d, f);
      case 3:
        return a.call(this, d, f, h)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  d.c = c;
  d.a = b;
  d.d = a;
  return d
}();
function Hc(a, b, c, d) {
  this.Z = a;
  this.da = b;
  this.h = c;
  this.k = d;
  this.e = 31850732;
  this.m = 1536
}
r = Hc.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.U = function() {
  if(1 < La(this.Z)) {
    return new Hc(wb(this.Z), this.da, this.h, null)
  }
  var a = mb(this.da);
  return null == a ? null : a
};
r.G = function(a, b) {
  return O(b, a)
};
r.toString = function() {
  return Ab(this)
};
r.v = aa();
r.N = function() {
  return A.a(this.Z, 0)
};
r.V = function() {
  return 1 < La(this.Z) ? new Hc(wb(this.Z), this.da, this.h, null) : null == this.da ? L : this.da
};
r.ob = function() {
  return null == this.da ? null : this.da
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new Hc(this.Z, this.da, b, this.k)
};
r.w = g("h");
r.J = function() {
  return $b(L, this.h)
};
r.Sa = g("Z");
r.Ia = function() {
  return null == this.da ? L : this.da
};
function Ic(a, b) {
  return 0 === La(a) ? b : new Hc(a, b, null, null)
}
function Jc(a) {
  for(var b = [];;) {
    if(H(a)) {
      b.push(I(a)), a = M(a)
    }else {
      return b
    }
  }
}
function Kc(a, b) {
  if(Lb(a)) {
    return P(a)
  }
  for(var c = a, d = b, e = 0;;) {
    var f;
    f = (f = 0 < d) ? H(c) : f;
    if(v(f)) {
      c = M(c), d -= 1, e += 1
    }else {
      return e
    }
  }
}
var Mc = function Lc(b) {
  return null == b ? null : null == M(b) ? H(I(b)) : x ? O(I(b), Lc(M(b))) : null
}, Nc = function() {
  function a(a, b, c, d) {
    return O(a, O(b, O(c, d)))
  }
  function b(a, b, c) {
    return O(a, O(b, c))
  }
  var c = null, d = function() {
    function a(c, d, e, m, n) {
      var q = null;
      4 < arguments.length && (q = N(Array.prototype.slice.call(arguments, 4), 0));
      return b.call(this, c, d, e, m, q)
    }
    function b(a, c, d, e, f) {
      return O(a, O(c, O(d, O(e, Mc(f)))))
    }
    a.n = 4;
    a.l = function(a) {
      var c = I(a);
      a = M(a);
      var d = I(a);
      a = M(a);
      var e = I(a);
      a = M(a);
      var n = I(a);
      a = J(a);
      return b(c, d, e, n, a)
    };
    a.g = b;
    return a
  }(), c = function(c, f, h, k, l) {
    switch(arguments.length) {
      case 1:
        return H(c);
      case 2:
        return O(c, f);
      case 3:
        return b.call(this, c, f, h);
      case 4:
        return a.call(this, c, f, h, k);
      default:
        return d.g(c, f, h, k, N(arguments, 4))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.n = 4;
  c.l = d.l;
  c.c = function(a) {
    return H(a)
  };
  c.a = function(a, b) {
    return O(a, b)
  };
  c.d = b;
  c.p = a;
  c.g = d.g;
  return c
}();
function Oc(a, b, c) {
  var d = H(c);
  if(0 === b) {
    return a.i ? a.i() : a.call(null)
  }
  c = B(d);
  var e = C(d);
  if(1 === b) {
    return a.c ? a.c(c) : a.c ? a.c(c) : a.call(null, c)
  }
  var d = B(e), f = C(e);
  if(2 === b) {
    return a.a ? a.a(c, d) : a.a ? a.a(c, d) : a.call(null, c, d)
  }
  var e = B(f), h = C(f);
  if(3 === b) {
    return a.d ? a.d(c, d, e) : a.d ? a.d(c, d, e) : a.call(null, c, d, e)
  }
  var f = B(h), k = C(h);
  if(4 === b) {
    return a.p ? a.p(c, d, e, f) : a.p ? a.p(c, d, e, f) : a.call(null, c, d, e, f)
  }
  h = B(k);
  k = C(k);
  if(5 === b) {
    return a.O ? a.O(c, d, e, f, h) : a.O ? a.O(c, d, e, f, h) : a.call(null, c, d, e, f, h)
  }
  a = B(k);
  var l = C(k);
  if(6 === b) {
    return a.ga ? a.ga(c, d, e, f, h, a) : a.ga ? a.ga(c, d, e, f, h, a) : a.call(null, c, d, e, f, h, a)
  }
  var k = B(l), m = C(l);
  if(7 === b) {
    return a.ta ? a.ta(c, d, e, f, h, a, k) : a.ta ? a.ta(c, d, e, f, h, a, k) : a.call(null, c, d, e, f, h, a, k)
  }
  var l = B(m), n = C(m);
  if(8 === b) {
    return a.fb ? a.fb(c, d, e, f, h, a, k, l) : a.fb ? a.fb(c, d, e, f, h, a, k, l) : a.call(null, c, d, e, f, h, a, k, l)
  }
  var m = B(n), q = C(n);
  if(9 === b) {
    return a.gb ? a.gb(c, d, e, f, h, a, k, l, m) : a.gb ? a.gb(c, d, e, f, h, a, k, l, m) : a.call(null, c, d, e, f, h, a, k, l, m)
  }
  var n = B(q), s = C(q);
  if(10 === b) {
    return a.Ua ? a.Ua(c, d, e, f, h, a, k, l, m, n) : a.Ua ? a.Ua(c, d, e, f, h, a, k, l, m, n) : a.call(null, c, d, e, f, h, a, k, l, m, n)
  }
  var q = B(s), t = C(s);
  if(11 === b) {
    return a.Va ? a.Va(c, d, e, f, h, a, k, l, m, n, q) : a.Va ? a.Va(c, d, e, f, h, a, k, l, m, n, q) : a.call(null, c, d, e, f, h, a, k, l, m, n, q)
  }
  var s = B(t), E = C(t);
  if(12 === b) {
    return a.Wa ? a.Wa(c, d, e, f, h, a, k, l, m, n, q, s) : a.Wa ? a.Wa(c, d, e, f, h, a, k, l, m, n, q, s) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s)
  }
  var t = B(E), Q = C(E);
  if(13 === b) {
    return a.Xa ? a.Xa(c, d, e, f, h, a, k, l, m, n, q, s, t) : a.Xa ? a.Xa(c, d, e, f, h, a, k, l, m, n, q, s, t) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t)
  }
  var E = B(Q), Z = C(Q);
  if(14 === b) {
    return a.Ya ? a.Ya(c, d, e, f, h, a, k, l, m, n, q, s, t, E) : a.Ya ? a.Ya(c, d, e, f, h, a, k, l, m, n, q, s, t, E) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t, E)
  }
  var Q = B(Z), K = C(Z);
  if(15 === b) {
    return a.Za ? a.Za(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q) : a.Za ? a.Za(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q)
  }
  var Z = B(K), U = C(K);
  if(16 === b) {
    return a.$a ? a.$a(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z) : a.$a ? a.$a(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z)
  }
  var K = B(U), ma = C(U);
  if(17 === b) {
    return a.ab ? a.ab(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K) : a.ab ? a.ab(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K)
  }
  var U = B(ma), Wb = C(ma);
  if(18 === b) {
    return a.bb ? a.bb(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U) : a.bb ? a.bb(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U)
  }
  ma = B(Wb);
  Wb = C(Wb);
  if(19 === b) {
    return a.cb ? a.cb(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U, ma) : a.cb ? a.cb(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U, ma) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U, ma)
  }
  var vb = B(Wb);
  C(Wb);
  if(20 === b) {
    return a.eb ? a.eb(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U, ma, vb) : a.eb ? a.eb(c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U, ma, vb) : a.call(null, c, d, e, f, h, a, k, l, m, n, q, s, t, E, Q, Z, K, U, ma, vb)
  }
  throw Error("Only up to 20 arguments supported on functions");
}
var Zb = function() {
  function a(a, b, c, d, e) {
    b = Nc.p(b, c, d, e);
    c = a.n;
    return a.l ? (d = Kc(b, c + 1), d <= c ? Oc(a, d, b) : a.l(b)) : a.apply(a, Jc(b))
  }
  function b(a, b, c, d) {
    b = Nc.d(b, c, d);
    c = a.n;
    return a.l ? (d = Kc(b, c + 1), d <= c ? Oc(a, d, b) : a.l(b)) : a.apply(a, Jc(b))
  }
  function c(a, b, c) {
    b = Nc.a(b, c);
    c = a.n;
    if(a.l) {
      var d = Kc(b, c + 1);
      return d <= c ? Oc(a, d, b) : a.l(b)
    }
    return a.apply(a, Jc(b))
  }
  function d(a, b) {
    var c = a.n;
    if(a.l) {
      var d = Kc(b, c + 1);
      return d <= c ? Oc(a, d, b) : a.l(b)
    }
    return a.apply(a, Jc(b))
  }
  var e = null, f = function() {
    function a(c, d, e, f, h, t) {
      var E = null;
      5 < arguments.length && (E = N(Array.prototype.slice.call(arguments, 5), 0));
      return b.call(this, c, d, e, f, h, E)
    }
    function b(a, c, d, e, f, h) {
      c = O(c, O(d, O(e, O(f, Mc(h)))));
      d = a.n;
      return a.l ? (e = Kc(c, d + 1), e <= d ? Oc(a, e, c) : a.l(c)) : a.apply(a, Jc(c))
    }
    a.n = 5;
    a.l = function(a) {
      var c = I(a);
      a = M(a);
      var d = I(a);
      a = M(a);
      var e = I(a);
      a = M(a);
      var f = I(a);
      a = M(a);
      var h = I(a);
      a = J(a);
      return b(c, d, e, f, h, a)
    };
    a.g = b;
    return a
  }(), e = function(e, k, l, m, n, q) {
    switch(arguments.length) {
      case 2:
        return d.call(this, e, k);
      case 3:
        return c.call(this, e, k, l);
      case 4:
        return b.call(this, e, k, l, m);
      case 5:
        return a.call(this, e, k, l, m, n);
      default:
        return f.g(e, k, l, m, n, N(arguments, 5))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  e.n = 5;
  e.l = f.l;
  e.a = d;
  e.d = c;
  e.p = b;
  e.O = a;
  e.g = f.g;
  return e
}();
function Pc(a, b) {
  for(;;) {
    if(null == H(b)) {
      return!0
    }
    if(v(a.c ? a.c(I(b)) : a.call(null, I(b)))) {
      var c = a, d = M(b);
      a = c;
      b = d
    }else {
      return x ? !1 : null
    }
  }
}
function Qc(a) {
  for(var b = Rc;;) {
    if(H(a)) {
      var c = b.c ? b.c(I(a)) : b.call(null, I(a));
      if(v(c)) {
        return c
      }
      a = M(a)
    }else {
      return null
    }
  }
}
function Rc(a) {
  return a
}
var Sc = function() {
  function a(a, b, c, e) {
    return new Cc(null, function() {
      var m = H(b), n = H(c), q = H(e);
      return(m ? n ? q : n : m) ? O(a.d ? a.d(I(m), I(n), I(q)) : a.call(null, I(m), I(n), I(q)), d.p(a, J(m), J(n), J(q))) : null
    }, null, null)
  }
  function b(a, b, c) {
    return new Cc(null, function() {
      var e = H(b), m = H(c);
      return(e ? m : e) ? O(a.a ? a.a(I(e), I(m)) : a.call(null, I(e), I(m)), d.d(a, J(e), J(m))) : null
    }, null, null)
  }
  function c(a, b) {
    return new Cc(null, function() {
      var c = H(b);
      if(c) {
        if(ic(c)) {
          for(var e = xb(c), m = P(e), n = new Ec(Array(m), 0), q = 0;;) {
            if(q < m) {
              var s = a.c ? a.c(A.a(e, q)) : a.call(null, A.a(e, q));
              n.add(s);
              q += 1
            }else {
              break
            }
          }
          return Ic(n.Z(), d.a(a, yb(c)))
        }
        return O(a.c ? a.c(I(c)) : a.call(null, I(c)), d.a(a, J(c)))
      }
      return null
    }, null, null)
  }
  var d = null, e = function() {
    function a(c, d, e, f, q) {
      var s = null;
      4 < arguments.length && (s = N(Array.prototype.slice.call(arguments, 4), 0));
      return b.call(this, c, d, e, f, s)
    }
    function b(a, c, e, f, h) {
      return d.a(function(b) {
        return Zb.a(a, b)
      }, function t(a) {
        return new Cc(null, function() {
          var b = d.a(H, a);
          return Pc(Rc, b) ? O(d.a(I, b), t(d.a(J, b))) : null
        }, null, null)
      }(Rb.g(h, f, N([e, c], 0))))
    }
    a.n = 4;
    a.l = function(a) {
      var c = I(a);
      a = M(a);
      var d = I(a);
      a = M(a);
      var e = I(a);
      a = M(a);
      var f = I(a);
      a = J(a);
      return b(c, d, e, f, a)
    };
    a.g = b;
    return a
  }(), d = function(d, h, k, l, m) {
    switch(arguments.length) {
      case 2:
        return c.call(this, d, h);
      case 3:
        return b.call(this, d, h, k);
      case 4:
        return a.call(this, d, h, k, l);
      default:
        return e.g(d, h, k, l, N(arguments, 4))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  d.n = 4;
  d.l = e.l;
  d.a = c;
  d.d = b;
  d.p = a;
  d.g = e.g;
  return d
}(), Uc = function Tc(b, c) {
  return new Cc(null, function() {
    if(0 < b) {
      var d = H(c);
      return d ? O(I(d), Tc(b - 1, J(d))) : null
    }
    return null
  }, null, null)
}, Vc = function() {
  function a(a, b) {
    return Uc(a, c.c(b))
  }
  function b(a) {
    return new Cc(null, function() {
      return O(a.i ? a.i() : a.call(null), c.c(a))
    }, null, null)
  }
  var c = null, c = function(c, e) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 2:
        return a.call(this, c, e)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.a = a;
  return c
}();
function Wc(a, b) {
  var c;
  null != a ? (c = a ? ((c = a.m & 4) ? c : a.yc) ? !0 : !1 : !1, c ? (c = qc.d(sb, rb(a), b), c = tb(c)) : c = qc.d(Oa, a, b)) : c = qc.d(Rb, L, b);
  return c
}
function Xc(a, b) {
  this.o = a;
  this.b = b
}
function Yc(a) {
  a = a.f;
  return 32 > a ? 0 : a - 1 >>> 5 << 5
}
function Zc(a, b, c) {
  for(;;) {
    if(0 === b) {
      return c
    }
    var d = new Xc(a, Array(32));
    d.b[0] = c;
    c = d;
    b -= 5
  }
}
var ad = function $c(b, c, d, e) {
  var f = new Xc(d.o, d.b.slice()), h = b.f - 1 >>> c & 31;
  5 === c ? f.b[h] = e : (d = d.b[h], b = null != d ? $c(b, c - 5, d, e) : Zc(null, c - 5, e), f.b[h] = b);
  return f
};
function bd(a, b) {
  throw Error([z("No item "), z(a), z(" in vector of length "), z(b)].join(""));
}
function cd(a, b) {
  var c = 0 <= b;
  if(c ? b < a.f : c) {
    if(b >= Yc(a)) {
      return a.q
    }
    for(var c = a.root, d = a.shift;;) {
      if(0 < d) {
        var e = d - 5, c = c.b[b >>> d & 31], d = e
      }else {
        return c.b
      }
    }
  }else {
    return bd(b, a.f)
  }
}
var ed = function dd(b, c, d, e, f) {
  var h = new Xc(d.o, d.b.slice());
  if(0 === c) {
    h.b[e & 31] = f
  }else {
    var k = e >>> c & 31;
    b = dd(b, c - 5, d.b[k], e, f);
    h.b[k] = b
  }
  return h
};
function fd(a, b, c, d, e, f) {
  this.h = a;
  this.f = b;
  this.shift = c;
  this.root = d;
  this.q = e;
  this.k = f;
  this.m = 4;
  this.e = 167668511
}
r = fd.prototype;
r.Aa = function() {
  return new gd(this.f, this.shift, hd.c ? hd.c(this.root) : hd.call(null, this.root), id.c ? id.c(this.q) : id.call(null, this.q))
};
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.Q = function(a, b) {
  return a.P(a, b, null)
};
r.D = function(a, b, c) {
  return a.P(a, b, c)
};
r.fa = function(a, b, c) {
  var d = 0 <= b;
  if(d ? b < this.f : d) {
    return Yc(a) <= b ? (a = this.q.slice(), a[b & 31] = c, new fd(this.h, this.f, this.shift, this.root, a, null)) : new fd(this.h, this.f, this.shift, ed(a, this.shift, this.root, b, c), this.q, null)
  }
  if(b === this.f) {
    return a.G(a, c)
  }
  if(x) {
    throw Error([z("Index "), z(b), z(" out of bounds  [0,"), z(this.f), z("]")].join(""));
  }
  return null
};
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return this.K(this, c);
      case 3:
        return this.P(this, c, d)
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.G = function(a, b) {
  if(32 > this.f - Yc(a)) {
    var c = this.q.slice();
    c.push(b);
    return new fd(this.h, this.f + 1, this.shift, this.root, c, null)
  }
  var d = this.f >>> 5 > 1 << this.shift, c = d ? this.shift + 5 : this.shift;
  if(d) {
    d = new Xc(null, Array(32));
    d.b[0] = this.root;
    var e = Zc(null, this.shift, new Xc(null, this.q));
    d.b[1] = e
  }else {
    d = ad(a, this.shift, this.root, new Xc(null, this.q))
  }
  return new fd(this.h, this.f + 1, c, d, [b], null)
};
r.ib = function(a) {
  return a.K(a, 0)
};
r.sb = function(a) {
  return a.K(a, 1)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return Jb.a(a, b)
};
r.S = function(a, b, c) {
  return Jb.d(a, b, c)
};
r.v = function(a) {
  return 0 === this.f ? null : 32 > this.f ? N.c(this.q) : x ? V.d ? V.d(a, 0, 0) : V.call(null, a, 0, 0) : null
};
r.I = g("f");
r.jb = function(a, b, c) {
  return a.fa(a, b, c)
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new fd(b, this.f, this.shift, this.root, this.q, this.k)
};
r.w = g("h");
r.K = function(a, b) {
  return cd(a, b)[b & 31]
};
r.P = function(a, b, c) {
  var d = 0 <= b;
  return(d ? b < this.f : d) ? a.K(a, b) : c
};
r.J = function() {
  return $b(jd, this.h)
};
var kd = new Xc(null, Array(32)), jd = new fd(null, 0, 5, kd, [], 0);
function W(a) {
  var b = a.length;
  if(32 > b) {
    return new fd(null, b, 5, kd, a, null)
  }
  for(var c = a.slice(0, 32), d = 32, e = rb(new fd(null, 32, 5, kd, c, null));;) {
    if(d < b) {
      c = d + 1, e = sb(e, a[d]), d = c
    }else {
      return tb(e)
    }
  }
}
function ld(a) {
  return tb(qc.d(sb, rb(jd), a))
}
function md(a, b, c, d, e, f) {
  this.F = a;
  this.X = b;
  this.j = c;
  this.s = d;
  this.h = e;
  this.k = f;
  this.e = 32243948;
  this.m = 1536
}
r = md.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.U = function(a) {
  return this.s + 1 < this.X.length ? (a = V.p ? V.p(this.F, this.X, this.j, this.s + 1) : V.call(null, this.F, this.X, this.j, this.s + 1), null == a ? null : a) : a.ob(a)
};
r.G = function(a, b) {
  return O(b, a)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return Jb.a(nd.d ? nd.d(this.F, this.j + this.s, P(this.F)) : nd.call(null, this.F, this.j + this.s, P(this.F)), b)
};
r.S = function(a, b, c) {
  return Jb.d(nd.d ? nd.d(this.F, this.j + this.s, P(this.F)) : nd.call(null, this.F, this.j + this.s, P(this.F)), b, c)
};
r.v = aa();
r.N = function() {
  return this.X[this.s]
};
r.V = function(a) {
  return this.s + 1 < this.X.length ? (a = V.p ? V.p(this.F, this.X, this.j, this.s + 1) : V.call(null, this.F, this.X, this.j, this.s + 1), null == a ? L : a) : a.Ia(a)
};
r.ob = function() {
  var a = this.X.length, a = this.j + a < La(this.F) ? V.d ? V.d(this.F, this.j + a, 0) : V.call(null, this.F, this.j + a, 0) : null;
  return null == a ? null : a
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return V.O ? V.O(this.F, this.X, this.j, this.s, b) : V.call(null, this.F, this.X, this.j, this.s, b)
};
r.J = function() {
  return $b(jd, this.h)
};
r.Sa = function() {
  return Gc.a(this.X, this.s)
};
r.Ia = function() {
  var a = this.X.length, a = this.j + a < La(this.F) ? V.d ? V.d(this.F, this.j + a, 0) : V.call(null, this.F, this.j + a, 0) : null;
  return null == a ? L : a
};
var V = function() {
  function a(a, b, c, d, l) {
    return new md(a, b, c, d, l, null)
  }
  function b(a, b, c, d) {
    return new md(a, b, c, d, null, null)
  }
  function c(a, b, c) {
    return new md(a, cd(a, b), b, c, null, null)
  }
  var d = null, d = function(d, f, h, k, l) {
    switch(arguments.length) {
      case 3:
        return c.call(this, d, f, h);
      case 4:
        return b.call(this, d, f, h, k);
      case 5:
        return a.call(this, d, f, h, k, l)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  d.d = c;
  d.p = b;
  d.O = a;
  return d
}();
function od(a, b, c, d, e) {
  this.h = a;
  this.ea = b;
  this.start = c;
  this.end = d;
  this.k = e;
  this.m = 0;
  this.e = 32400159
}
r = od.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.Q = function(a, b) {
  return a.P(a, b, null)
};
r.D = function(a, b, c) {
  return a.P(a, b, c)
};
r.fa = function(a, b, c) {
  var d = this, e = d.start + b;
  return pd.O ? pd.O(d.h, Vb.d(d.ea, e, c), d.start, function() {
    var a = d.end, b = e + 1;
    return a > b ? a : b
  }(), null) : pd.call(null, d.h, Vb.d(d.ea, e, c), d.start, function() {
    var a = d.end, b = e + 1;
    return a > b ? a : b
  }(), null)
};
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return this.K(this, c);
      case 3:
        return this.P(this, c, d)
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.G = function(a, b) {
  return pd.O ? pd.O(this.h, bb(this.ea, this.end, b), this.start, this.end + 1, null) : pd.call(null, this.h, bb(this.ea, this.end, b), this.start, this.end + 1, null)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return Jb.a(a, b)
};
r.S = function(a, b, c) {
  return Jb.d(a, b, c)
};
r.v = function() {
  var a = this;
  return function c(d) {
    return d === a.end ? null : O(A.a(a.ea, d), new Cc(null, function() {
      return c(d + 1)
    }, null, null))
  }(a.start)
};
r.I = function() {
  return this.end - this.start
};
r.jb = function(a, b, c) {
  return a.fa(a, b, c)
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return pd.O ? pd.O(b, this.ea, this.start, this.end, this.k) : pd.call(null, b, this.ea, this.start, this.end, this.k)
};
r.w = g("h");
r.K = function(a, b) {
  var c = 0 > b;
  return(c ? c : this.end <= this.start + b) ? bd(b, this.end - this.start) : A.a(this.ea, this.start + b)
};
r.P = function(a, b, c) {
  return((a = 0 > b) ? a : this.end <= this.start + b) ? c : A.d(this.ea, this.start + b, c)
};
r.J = function() {
  return $b(jd, this.h)
};
function pd(a, b, c, d, e) {
  for(;;) {
    if(b instanceof od) {
      var f = b.start + c, h = b.start + d;
      b = b.ea;
      c = f;
      d = h
    }else {
      var k = P(b);
      if(function() {
        var a = 0 > c;
        return a || (a = 0 > d) ? a : (a = c > k) ? a : d > k
      }()) {
        throw Error("Index out of bounds");
      }
      return new od(a, b, c, d, e)
    }
  }
}
var nd = function() {
  function a(a, b, c) {
    return pd(null, a, b, c, null)
  }
  function b(a, b) {
    return c.d(a, b, P(a))
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}();
function hd(a) {
  return new Xc({}, a.b.slice())
}
function id(a) {
  var b = Array(32);
  kc(a, 0, b, 0, a.length);
  return b
}
var sd = function qd(b, c, d, e) {
  d = b.root.o === d.o ? d : new Xc(b.root.o, d.b.slice());
  var f = b.f - 1 >>> c & 31;
  if(5 === c) {
    b = e
  }else {
    var h = d.b[f];
    b = null != h ? qd(b, c - 5, h, e) : Zc(b.root.o, c - 5, e)
  }
  d.b[f] = b;
  return d
};
function gd(a, b, c, d) {
  this.f = a;
  this.shift = b;
  this.root = c;
  this.q = d;
  this.e = 275;
  this.m = 88
}
r = gd.prototype;
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return this.Q(this, c);
      case 3:
        return this.D(this, c, d)
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.Q = function(a, b) {
  return a.P(a, b, null)
};
r.D = function(a, b, c) {
  return a.P(a, b, c)
};
r.K = function(a, b) {
  if(this.root.o) {
    return cd(a, b)[b & 31]
  }
  throw Error("nth after persistent!");
};
r.P = function(a, b, c) {
  var d = 0 <= b;
  return(d ? b < this.f : d) ? a.K(a, b) : c
};
r.I = function() {
  if(this.root.o) {
    return this.f
  }
  throw Error("count after persistent!");
};
function td(a, b, c, d) {
  if(a.root.o) {
    if(function() {
      var b = 0 <= c;
      return b ? c < a.f : b
    }()) {
      if(Yc(b) <= c) {
        a.q[c & 31] = d
      }else {
        var e = function h(b, e) {
          var m = a.root.o === e.o ? e : new Xc(a.root.o, e.b.slice());
          if(0 === b) {
            m.b[c & 31] = d
          }else {
            var n = c >>> b & 31, q = h(b - 5, m.b[n]);
            m.b[n] = q
          }
          return m
        }.call(null, a.shift, a.root);
        a.root = e
      }
      return b
    }
    if(c === a.f) {
      return b.ia(b, d)
    }
    if(x) {
      throw Error([z("Index "), z(c), z(" out of bounds for TransientVector of length"), z(a.f)].join(""));
    }
    return null
  }
  throw Error("assoc! after persistent!");
}
r.ua = function(a, b, c) {
  return td(a, a, b, c)
};
r.ia = function(a, b) {
  if(this.root.o) {
    if(32 > this.f - Yc(a)) {
      this.q[this.f & 31] = b
    }else {
      var c = new Xc(this.root.o, this.q), d = Array(32);
      d[0] = b;
      this.q = d;
      if(this.f >>> 5 > 1 << this.shift) {
        var d = Array(32), e = this.shift + 5;
        d[0] = this.root;
        d[1] = Zc(this.root.o, this.shift, c);
        this.root = new Xc(this.root.o, d);
        this.shift = e
      }else {
        this.root = sd(a, this.shift, this.root, c)
      }
    }
    this.f += 1;
    return a
  }
  throw Error("conj! after persistent!");
};
r.oa = function(a) {
  if(this.root.o) {
    this.root.o = null;
    a = this.f - Yc(a);
    var b = Array(a);
    kc(this.q, 0, b, 0, a);
    return new fd(null, this.f, this.shift, this.root, b, null)
  }
  throw Error("persistent! called twice");
};
function ud() {
  this.m = 0;
  this.e = 2097152
}
ud.prototype.u = p(!1);
var vd = new ud;
function wd(a, b) {
  return nc(gc(b) ? P(a) === P(b) ? Pc(Rc, Sc.a(function(a) {
    return Ib.a(Tb.d(b, I(a), vd), I(M(a)))
  }, a)) : null : null)
}
function xd(a, b) {
  var c = a.b;
  if(b instanceof T) {
    a: {
      for(var d = c.length, e = b.la, f = 0;;) {
        if(d <= f) {
          c = -1;
          break a
        }
        var h = c[f], k = h instanceof T;
        if(k ? e === h.la : k) {
          c = f;
          break a
        }
        if(x) {
          f += 2
        }else {
          c = null;
          break a
        }
      }
      c = void 0
    }
  }else {
    if((d = da(b)) ? d : "number" === typeof b) {
      a: {
        d = c.length;
        for(e = 0;;) {
          if(d <= e) {
            c = -1;
            break a
          }
          if(b === c[e]) {
            c = e;
            break a
          }
          if(x) {
            e += 2
          }else {
            c = null;
            break a
          }
        }
        c = void 0
      }
    }else {
      if(b instanceof F) {
        a: {
          d = c.length;
          e = b.ra;
          for(f = 0;;) {
            if(d <= f) {
              c = -1;
              break a
            }
            h = c[f];
            if((k = h instanceof F) ? e === h.ra : k) {
              c = f;
              break a
            }
            if(x) {
              f += 2
            }else {
              c = null;
              break a
            }
          }
          c = void 0
        }
      }else {
        if(null == b) {
          a: {
            d = c.length;
            for(e = 0;;) {
              if(d <= e) {
                c = -1;
                break a
              }
              if(null == c[e]) {
                c = e;
                break a
              }
              if(x) {
                e += 2
              }else {
                c = null;
                break a
              }
            }
            c = void 0
          }
        }else {
          if(x) {
            a: {
              d = c.length;
              for(e = 0;;) {
                if(d <= e) {
                  c = -1;
                  break a
                }
                if(Ib.a(b, c[e])) {
                  c = e;
                  break a
                }
                if(x) {
                  e += 2
                }else {
                  c = null;
                  break a
                }
              }
              c = void 0
            }
          }else {
            c = null
          }
        }
      }
    }
  }
  return c
}
function yd(a, b, c) {
  this.b = a;
  this.j = b;
  this.Y = c;
  this.m = 0;
  this.e = 32374990
}
r = yd.prototype;
r.C = function(a) {
  return Nb(a)
};
r.U = function() {
  return this.j < this.b.length - 2 ? new yd(this.b, this.j + 2, this.Y) : null
};
r.G = function(a, b) {
  return O(b, a)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = aa();
r.I = function() {
  return(this.b.length - this.j) / 2
};
r.N = function() {
  return W([this.b[this.j], this.b[this.j + 1]])
};
r.V = function() {
  return this.j < this.b.length - 2 ? new yd(this.b, this.j + 2, this.Y) : L
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new yd(this.b, this.j, b)
};
r.w = g("Y");
r.J = function() {
  return $b(L, this.Y)
};
function zd(a, b, c, d) {
  this.h = a;
  this.f = b;
  this.b = c;
  this.k = d;
  this.m = 4;
  this.e = 16123663
}
r = zd.prototype;
r.Aa = function() {
  return new Ad({}, this.b.length, this.b.slice())
};
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = tc(a)
};
r.Q = function(a, b) {
  return a.D(a, b, null)
};
r.D = function(a, b, c) {
  a = xd(a, b);
  return-1 === a ? c : this.b[a + 1]
};
r.fa = function(a, b, c) {
  var d = xd(a, b);
  if(-1 === d) {
    if(this.f < Bd) {
      d = a.b;
      a = d.length;
      for(var e = Array(a + 2), f = 0;;) {
        if(f < a) {
          e[f] = d[f], f += 1
        }else {
          break
        }
      }
      e[a] = b;
      e[a + 1] = c;
      return new zd(this.h, this.f + 1, e, null)
    }
    return gb(Va(Wc(Cd, a), b, c), this.h)
  }
  return c === this.b[d + 1] ? a : x ? (b = this.b.slice(), b[d + 1] = c, new zd(this.h, this.f, b, null)) : null
};
r.Ra = function(a, b) {
  return-1 !== xd(a, b)
};
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return this.Q(this, c);
      case 3:
        return this.D(this, c, d)
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.G = function(a, b) {
  return hc(b) ? a.fa(a, A.a(b, 0), A.a(b, 1)) : qc.d(Oa, a, b)
};
r.toString = function() {
  return Ab(this)
};
r.v = function() {
  return 0 <= this.b.length - 2 ? new yd(this.b, 0, null) : null
};
r.I = g("f");
r.u = function(a, b) {
  return wd(a, b)
};
r.A = function(a, b) {
  return new zd(b, this.f, this.b, this.k)
};
r.w = g("h");
r.J = function() {
  return gb(Dd, this.h)
};
var Dd = new zd(null, 0, [], null), Bd = 8;
function Bb(a) {
  return new zd(null, a.length / 2, a, null)
}
function Ad(a, b, c) {
  this.va = a;
  this.pa = b;
  this.b = c;
  this.m = 56;
  this.e = 258
}
r = Ad.prototype;
r.ua = function(a, b, c) {
  if(v(this.va)) {
    var d = xd(a, b);
    if(-1 === d) {
      if(this.pa + 2 <= 2 * Bd) {
        return this.pa += 2, this.b.push(b), this.b.push(c), a
      }
      a = Ed.a ? Ed.a(this.pa, this.b) : Ed.call(null, this.pa, this.b);
      return ub(a, b, c)
    }
    c !== this.b[d + 1] && (this.b[d + 1] = c);
    return a
  }
  throw Error("assoc! after persistent!");
};
r.ia = function(a, b) {
  if(v(this.va)) {
    var c;
    c = b ? ((c = b.e & 2048) ? c : b.Lb) ? !0 : b.e ? !1 : w(Xa, b) : w(Xa, b);
    if(c) {
      return a.ua(a, uc.c ? uc.c(b) : uc.call(null, b), vc.c ? vc.c(b) : vc.call(null, b))
    }
    c = H(b);
    for(var d = a;;) {
      var e = I(c);
      if(v(e)) {
        c = M(c), d = d.ua(d, uc.c ? uc.c(e) : uc.call(null, e), vc.c ? vc.c(e) : vc.call(null, e))
      }else {
        return d
      }
    }
  }else {
    throw Error("conj! after persistent!");
  }
};
r.oa = function() {
  if(v(this.va)) {
    return this.va = !1, new zd(null, rc(this.pa), this.b, null)
  }
  throw Error("persistent! called twice");
};
r.Q = function(a, b) {
  return a.D(a, b, null)
};
r.D = function(a, b, c) {
  if(v(this.va)) {
    return a = xd(a, b), -1 === a ? c : this.b[a + 1]
  }
  throw Error("lookup after persistent!");
};
r.I = function() {
  if(v(this.va)) {
    return rc(this.pa)
  }
  throw Error("count after persistent!");
};
function Ed(a, b) {
  for(var c = rb(Cd), d = 0;;) {
    if(d < a) {
      c = ub(c, b[d], b[d + 1]), d += 2
    }else {
      return c
    }
  }
}
function Fd() {
  this.M = !1
}
function Gd(a, b) {
  return a === b ? !0 : zc(a, b) ? !0 : x ? Ib.a(a, b) : null
}
var Hd = function() {
  function a(a, b, c, h, k) {
    a = a.slice();
    a[b] = c;
    a[h] = k;
    return a
  }
  function b(a, b, c) {
    a = a.slice();
    a[b] = c;
    return a
  }
  var c = null, c = function(c, e, f, h, k) {
    switch(arguments.length) {
      case 3:
        return b.call(this, c, e, f);
      case 5:
        return a.call(this, c, e, f, h, k)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.d = b;
  c.O = a;
  return c
}(), Id = function() {
  function a(a, b, c, h, k, l) {
    a = a.wa(b);
    a.b[c] = h;
    a.b[k] = l;
    return a
  }
  function b(a, b, c, h) {
    a = a.wa(b);
    a.b[c] = h;
    return a
  }
  var c = null, c = function(c, e, f, h, k, l) {
    switch(arguments.length) {
      case 4:
        return b.call(this, c, e, f, h);
      case 6:
        return a.call(this, c, e, f, h, k, l)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.p = b;
  c.ga = a;
  return c
}();
function Jd(a, b, c) {
  this.o = a;
  this.B = b;
  this.b = c
}
r = Jd.prototype;
r.aa = function(a, b, c, d, e, f) {
  var h = 1 << (c >>> b & 31), k = sc(this.B & h - 1);
  if(0 === (this.B & h)) {
    var l = sc(this.B);
    if(2 * l < this.b.length) {
      a = this.wa(a);
      b = a.b;
      f.M = !0;
      a: {
        for(c = 2 * (l - k), f = 2 * k + (c - 1), l = 2 * (k + 1) + (c - 1);;) {
          if(0 === c) {
            break a
          }
          b[l] = b[f];
          l -= 1;
          c -= 1;
          f -= 1
        }
      }
      b[2 * k] = d;
      b[2 * k + 1] = e;
      a.B |= h;
      return a
    }
    if(16 <= l) {
      k = Array(32);
      k[c >>> b & 31] = Kd.aa(a, b + 5, c, d, e, f);
      for(e = d = 0;;) {
        if(32 > d) {
          0 !== (this.B >>> d & 1) && (k[d] = null != this.b[e] ? Kd.aa(a, b + 5, G.c(this.b[e]), this.b[e], this.b[e + 1], f) : this.b[e + 1], e += 2), d += 1
        }else {
          break
        }
      }
      return new Ld(a, l + 1, k)
    }
    return x ? (b = Array(2 * (l + 4)), kc(this.b, 0, b, 0, 2 * k), b[2 * k] = d, b[2 * k + 1] = e, kc(this.b, 2 * k, b, 2 * (k + 1), 2 * (l - k)), f.M = !0, a = this.wa(a), a.b = b, a.B |= h, a) : null
  }
  l = this.b[2 * k];
  h = this.b[2 * k + 1];
  return null == l ? (l = h.aa(a, b + 5, c, d, e, f), l === h ? this : Id.p(this, a, 2 * k + 1, l)) : Gd(d, l) ? e === h ? this : Id.p(this, a, 2 * k + 1, e) : x ? (f.M = !0, Id.ga(this, a, 2 * k, null, 2 * k + 1, Md.ta ? Md.ta(a, b + 5, l, h, c, d, e) : Md.call(null, a, b + 5, l, h, c, d, e))) : null
};
r.Fa = function() {
  return Nd.c ? Nd.c(this.b) : Nd.call(null, this.b)
};
r.wa = function(a) {
  if(a === this.o) {
    return this
  }
  var b = sc(this.B), c = Array(0 > b ? 4 : 2 * (b + 1));
  kc(this.b, 0, c, 0, 2 * b);
  return new Jd(a, this.B, c)
};
r.$ = function(a, b, c, d, e) {
  var f = 1 << (b >>> a & 31), h = sc(this.B & f - 1);
  if(0 === (this.B & f)) {
    var k = sc(this.B);
    if(16 <= k) {
      h = Array(32);
      h[b >>> a & 31] = Kd.$(a + 5, b, c, d, e);
      for(d = c = 0;;) {
        if(32 > c) {
          0 !== (this.B >>> c & 1) && (h[c] = null != this.b[d] ? Kd.$(a + 5, G.c(this.b[d]), this.b[d], this.b[d + 1], e) : this.b[d + 1], d += 2), c += 1
        }else {
          break
        }
      }
      return new Ld(null, k + 1, h)
    }
    a = Array(2 * (k + 1));
    kc(this.b, 0, a, 0, 2 * h);
    a[2 * h] = c;
    a[2 * h + 1] = d;
    kc(this.b, 2 * h, a, 2 * (h + 1), 2 * (k - h));
    e.M = !0;
    return new Jd(null, this.B | f, a)
  }
  k = this.b[2 * h];
  f = this.b[2 * h + 1];
  return null == k ? (k = f.$(a + 5, b, c, d, e), k === f ? this : new Jd(null, this.B, Hd.d(this.b, 2 * h + 1, k))) : Gd(c, k) ? d === f ? this : new Jd(null, this.B, Hd.d(this.b, 2 * h + 1, d)) : x ? (e.M = !0, new Jd(null, this.B, Hd.O(this.b, 2 * h, null, 2 * h + 1, Md.ga ? Md.ga(a + 5, k, f, b, c, d) : Md.call(null, a + 5, k, f, b, c, d)))) : null
};
r.ma = function(a, b, c, d) {
  var e = 1 << (b >>> a & 31);
  if(0 === (this.B & e)) {
    return d
  }
  var f = sc(this.B & e - 1), e = this.b[2 * f], f = this.b[2 * f + 1];
  return null == e ? f.ma(a + 5, b, c, d) : Gd(c, e) ? f : x ? d : null
};
var Kd = new Jd(null, 0, []);
function Ld(a, b, c) {
  this.o = a;
  this.f = b;
  this.b = c
}
r = Ld.prototype;
r.aa = function(a, b, c, d, e, f) {
  var h = c >>> b & 31, k = this.b[h];
  if(null == k) {
    return a = Id.p(this, a, h, Kd.aa(a, b + 5, c, d, e, f)), a.f += 1, a
  }
  b = k.aa(a, b + 5, c, d, e, f);
  return b === k ? this : Id.p(this, a, h, b)
};
r.Fa = function() {
  return Od.c ? Od.c(this.b) : Od.call(null, this.b)
};
r.wa = function(a) {
  return a === this.o ? this : new Ld(a, this.f, this.b.slice())
};
r.$ = function(a, b, c, d, e) {
  var f = b >>> a & 31, h = this.b[f];
  if(null == h) {
    return new Ld(null, this.f + 1, Hd.d(this.b, f, Kd.$(a + 5, b, c, d, e)))
  }
  a = h.$(a + 5, b, c, d, e);
  return a === h ? this : new Ld(null, this.f, Hd.d(this.b, f, a))
};
r.ma = function(a, b, c, d) {
  var e = this.b[b >>> a & 31];
  return null != e ? e.ma(a + 5, b, c, d) : d
};
function Pd(a, b, c) {
  b *= 2;
  for(var d = 0;;) {
    if(d < b) {
      if(Gd(c, a[d])) {
        return d
      }
      d += 2
    }else {
      return-1
    }
  }
}
function Qd(a, b, c, d) {
  this.o = a;
  this.ja = b;
  this.f = c;
  this.b = d
}
r = Qd.prototype;
r.aa = function(a, b, c, d, e, f) {
  if(c === this.ja) {
    b = Pd(this.b, this.f, d);
    if(-1 === b) {
      if(this.b.length > 2 * this.f) {
        return a = Id.ga(this, a, 2 * this.f, d, 2 * this.f + 1, e), f.M = !0, a.f += 1, a
      }
      c = this.b.length;
      b = Array(c + 2);
      kc(this.b, 0, b, 0, c);
      b[c] = d;
      b[c + 1] = e;
      f.M = !0;
      f = this.f + 1;
      a === this.o ? (this.b = b, this.f = f, a = this) : a = new Qd(this.o, this.ja, f, b);
      return a
    }
    return this.b[b + 1] === e ? this : Id.p(this, a, b + 1, e)
  }
  return(new Jd(a, 1 << (this.ja >>> b & 31), [null, this, null, null])).aa(a, b, c, d, e, f)
};
r.Fa = function() {
  return Nd.c ? Nd.c(this.b) : Nd.call(null, this.b)
};
r.wa = function(a) {
  if(a === this.o) {
    return this
  }
  var b = Array(2 * (this.f + 1));
  kc(this.b, 0, b, 0, 2 * this.f);
  return new Qd(a, this.ja, this.f, b)
};
r.$ = function(a, b, c, d, e) {
  return b === this.ja ? (a = Pd(this.b, this.f, c), -1 === a ? (a = this.b.length, b = Array(a + 2), kc(this.b, 0, b, 0, a), b[a] = c, b[a + 1] = d, e.M = !0, new Qd(null, this.ja, this.f + 1, b)) : Ib.a(this.b[a], d) ? this : new Qd(null, this.ja, this.f, Hd.d(this.b, a + 1, d))) : (new Jd(null, 1 << (this.ja >>> a & 31), [null, this])).$(a, b, c, d, e)
};
r.ma = function(a, b, c, d) {
  a = Pd(this.b, this.f, c);
  return 0 > a ? d : Gd(c, this.b[a]) ? this.b[a + 1] : x ? d : null
};
var Md = function() {
  function a(a, b, c, h, k, l, m) {
    var n = G.c(c);
    if(n === k) {
      return new Qd(null, n, 2, [c, h, l, m])
    }
    var q = new Fd;
    return Kd.aa(a, b, n, c, h, q).aa(a, b, k, l, m, q)
  }
  function b(a, b, c, h, k, l) {
    var m = G.c(b);
    if(m === h) {
      return new Qd(null, m, 2, [b, c, k, l])
    }
    var n = new Fd;
    return Kd.$(a, m, b, c, n).$(a, h, k, l, n)
  }
  var c = null, c = function(c, e, f, h, k, l, m) {
    switch(arguments.length) {
      case 6:
        return b.call(this, c, e, f, h, k, l);
      case 7:
        return a.call(this, c, e, f, h, k, l, m)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.ga = b;
  c.ta = a;
  return c
}();
function Rd(a, b, c, d, e) {
  this.h = a;
  this.ca = b;
  this.j = c;
  this.r = d;
  this.k = e;
  this.m = 0;
  this.e = 32374860
}
r = Rd.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.G = function(a, b) {
  return O(b, a)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = aa();
r.N = function() {
  return null == this.r ? W([this.ca[this.j], this.ca[this.j + 1]]) : I(this.r)
};
r.V = function() {
  return null == this.r ? Nd.d ? Nd.d(this.ca, this.j + 2, null) : Nd.call(null, this.ca, this.j + 2, null) : Nd.d ? Nd.d(this.ca, this.j, M(this.r)) : Nd.call(null, this.ca, this.j, M(this.r))
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new Rd(b, this.ca, this.j, this.r, this.k)
};
r.w = g("h");
r.J = function() {
  return $b(L, this.h)
};
var Nd = function() {
  function a(a, b, c) {
    if(null == c) {
      for(c = a.length;;) {
        if(b < c) {
          if(null != a[b]) {
            return new Rd(null, a, b, null, null)
          }
          var h = a[b + 1];
          if(v(h) && (h = h.Fa(), v(h))) {
            return new Rd(null, a, b + 2, h, null)
          }
          b += 2
        }else {
          return null
        }
      }
    }else {
      return new Rd(null, a, b, c, null)
    }
  }
  function b(a) {
    return c.d(a, 0, null)
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.d = a;
  return c
}();
function Sd(a, b, c, d, e) {
  this.h = a;
  this.ca = b;
  this.j = c;
  this.r = d;
  this.k = e;
  this.m = 0;
  this.e = 32374860
}
r = Sd.prototype;
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = Nb(a)
};
r.G = function(a, b) {
  return O(b, a)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = aa();
r.N = function() {
  return I(this.r)
};
r.V = function() {
  return Od.p ? Od.p(null, this.ca, this.j, M(this.r)) : Od.call(null, null, this.ca, this.j, M(this.r))
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new Sd(b, this.ca, this.j, this.r, this.k)
};
r.w = g("h");
r.J = function() {
  return $b(L, this.h)
};
var Od = function() {
  function a(a, b, c, h) {
    if(null == h) {
      for(h = b.length;;) {
        if(c < h) {
          var k = b[c];
          if(v(k) && (k = k.Fa(), v(k))) {
            return new Sd(a, b, c + 1, k, null)
          }
          c += 1
        }else {
          return null
        }
      }
    }else {
      return new Sd(a, b, c, h, null)
    }
  }
  function b(a) {
    return c.p(null, a, 0, null)
  }
  var c = null, c = function(c, e, f, h) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 4:
        return a.call(this, c, e, f, h)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.p = a;
  return c
}();
function Td(a, b, c, d, e, f) {
  this.h = a;
  this.f = b;
  this.root = c;
  this.T = d;
  this.W = e;
  this.k = f;
  this.m = 4;
  this.e = 16123663
}
r = Td.prototype;
r.Aa = function() {
  return new Ud({}, this.root, this.f, this.T, this.W)
};
r.C = function(a) {
  var b = this.k;
  return null != b ? b : this.k = a = tc(a)
};
r.Q = function(a, b) {
  return a.D(a, b, null)
};
r.D = function(a, b, c) {
  return null == b ? this.T ? this.W : c : null == this.root ? c : x ? this.root.ma(0, G.c(b), b, c) : null
};
r.fa = function(a, b, c) {
  if(null == b) {
    var d = this.T;
    return(d ? c === this.W : d) ? a : new Td(this.h, this.T ? this.f : this.f + 1, this.root, !0, c, null)
  }
  d = new Fd;
  c = (null == this.root ? Kd : this.root).$(0, G.c(b), b, c, d);
  return c === this.root ? a : new Td(this.h, d.M ? this.f + 1 : this.f, c, this.T, this.W, null)
};
r.Ra = function(a, b) {
  return null == b ? this.T : null == this.root ? !1 : x ? this.root.ma(0, G.c(b), b, lc) !== lc : null
};
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return this.Q(this, c);
      case 3:
        return this.D(this, c, d)
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.G = function(a, b) {
  return hc(b) ? a.fa(a, A.a(b, 0), A.a(b, 1)) : qc.d(Oa, a, b)
};
r.toString = function() {
  return Ab(this)
};
r.v = function() {
  if(0 < this.f) {
    var a = null != this.root ? this.root.Fa() : null;
    return this.T ? O(W([null, this.W]), a) : a
  }
  return null
};
r.I = g("f");
r.u = function(a, b) {
  return wd(a, b)
};
r.A = function(a, b) {
  return new Td(b, this.f, this.root, this.T, this.W, this.k)
};
r.w = g("h");
r.J = function() {
  return gb(Cd, this.h)
};
var Cd = new Td(null, 0, null, !1, null, 0);
function Ud(a, b, c, d, e) {
  this.o = a;
  this.root = b;
  this.count = c;
  this.T = d;
  this.W = e;
  this.m = 56;
  this.e = 258
}
r = Ud.prototype;
r.ua = function(a, b, c) {
  return Vd(a, b, c)
};
r.ia = function(a, b) {
  var c;
  a: {
    if(a.o) {
      c = b ? ((c = b.e & 2048) ? c : b.Lb) ? !0 : b.e ? !1 : w(Xa, b) : w(Xa, b);
      if(c) {
        c = Vd(a, uc.c ? uc.c(b) : uc.call(null, b), vc.c ? vc.c(b) : vc.call(null, b));
        break a
      }
      c = H(b);
      for(var d = a;;) {
        var e = I(c);
        if(v(e)) {
          c = M(c), d = Vd(d, uc.c ? uc.c(e) : uc.call(null, e), vc.c ? vc.c(e) : vc.call(null, e))
        }else {
          c = d;
          break a
        }
      }
    }else {
      throw Error("conj! after persistent");
    }
    c = void 0
  }
  return c
};
r.oa = function(a) {
  if(a.o) {
    a.o = null, a = new Td(null, a.count, a.root, a.T, a.W, null)
  }else {
    throw Error("persistent! called twice");
  }
  return a
};
r.Q = function(a, b) {
  return null == b ? this.T ? this.W : null : null == this.root ? null : this.root.ma(0, G.c(b), b)
};
r.D = function(a, b, c) {
  return null == b ? this.T ? this.W : c : null == this.root ? c : this.root.ma(0, G.c(b), b, c)
};
r.I = function() {
  if(this.o) {
    return this.count
  }
  throw Error("count after persistent!");
};
function Vd(a, b, c) {
  if(a.o) {
    if(null == b) {
      a.W !== c && (a.W = c), a.T || (a.count += 1, a.T = !0)
    }else {
      var d = new Fd;
      b = (null == a.root ? Kd : a.root).aa(a.o, 0, G.c(b), b, c, d);
      b !== a.root && (a.root = b);
      d.M && (a.count += 1)
    }
    return a
  }
  throw Error("assoc! after persistent!");
}
var Ub = function() {
  function a(a) {
    var d = null;
    0 < arguments.length && (d = N(Array.prototype.slice.call(arguments, 0), 0));
    return b.call(this, d)
  }
  function b(a) {
    for(var b = H(a), e = rb(Cd);;) {
      if(b) {
        a = M(M(b));
        var f = I(b), b = I(M(b)), e = ub(e, f, b), b = a
      }else {
        return tb(e)
      }
    }
  }
  a.n = 0;
  a.l = function(a) {
    a = H(a);
    return b(a)
  };
  a.g = b;
  return a
}(), Wd = function() {
  function a(a) {
    var d = null;
    0 < arguments.length && (d = N(Array.prototype.slice.call(arguments, 0), 0));
    return b.call(this, d)
  }
  function b(a) {
    return new zd(null, rc(P(a)), Zb.a(Ia, a), null)
  }
  a.n = 0;
  a.l = function(a) {
    a = H(a);
    return b(a)
  };
  a.g = b;
  return a
}();
function Xd(a, b) {
  this.ba = a;
  this.Y = b;
  this.m = 0;
  this.e = 32374988
}
r = Xd.prototype;
r.C = function(a) {
  return Nb(a)
};
r.U = function() {
  var a = this.ba;
  if(a) {
    var b = a.e & 128, a = (b ? b : a.tb) ? !0 : a.e ? !1 : w(Ra, a)
  }else {
    a = w(Ra, a)
  }
  a = a ? this.ba.U(this.ba) : M(this.ba);
  return null == a ? null : new Xd(a, this.Y)
};
r.G = function(a, b) {
  return O(b, a)
};
r.toString = function() {
  return Ab(this)
};
r.R = function(a, b) {
  return S.a(b, a)
};
r.S = function(a, b, c) {
  return S.d(b, c, a)
};
r.v = aa();
r.N = function() {
  var a = this.ba.N(this.ba);
  return a.ib(a)
};
r.V = function() {
  var a = this.ba;
  if(a) {
    var b = a.e & 128, a = (b ? b : a.tb) ? !0 : a.e ? !1 : w(Ra, a)
  }else {
    a = w(Ra, a)
  }
  a = a ? this.ba.U(this.ba) : M(this.ba);
  return null != a ? new Xd(a, this.Y) : L
};
r.u = function(a, b) {
  return Pb(a, b)
};
r.A = function(a, b) {
  return new Xd(this.ba, b)
};
r.w = g("Y");
r.J = function() {
  return $b(L, this.Y)
};
function uc(a) {
  return Ya(a)
}
function vc(a) {
  return Za(a)
}
var Yd = function() {
  function a(a) {
    var d = null;
    0 < arguments.length && (d = N(Array.prototype.slice.call(arguments, 0), 0));
    return b.call(this, d)
  }
  function b(a) {
    return v(Qc(a)) ? qc.a(function(a, b) {
      return Rb.a(v(a) ? a : Dd, b)
    }, a) : null
  }
  a.n = 0;
  a.l = function(a) {
    a = H(a);
    return b(a)
  };
  a.g = b;
  return a
}();
function Zd(a, b, c) {
  this.h = a;
  this.ya = b;
  this.k = c;
  this.m = 4;
  this.e = 15077647
}
r = Zd.prototype;
r.Aa = function() {
  return new $d(rb(this.ya))
};
r.C = function(a) {
  var b = this.k;
  if(null != b) {
    return b
  }
  a: {
    b = 0;
    for(a = H(a);;) {
      if(a) {
        var c = I(a), b = (b + G.c(c)) % 4503599627370496;
        a = M(a)
      }else {
        break a
      }
    }
    b = void 0
  }
  return this.k = b
};
r.Q = function(a, b) {
  return a.D(a, b, null)
};
r.D = function(a, b, c) {
  return v(Ua(this.ya, b)) ? b : c
};
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return this.Q(this, c);
      case 3:
        return this.D(this, c, d)
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.G = function(a, b) {
  return new Zd(this.h, Vb.d(this.ya, b, null), null)
};
r.toString = function() {
  return Ab(this)
};
r.v = function() {
  var a = H(this.ya);
  return a ? new Xd(a, null) : null
};
r.I = function() {
  return La(this.ya)
};
r.u = function(a, b) {
  var c = ec(b);
  return c ? (c = P(a) === P(b)) ? Pc(function(b) {
    return Tb.d(a, b, lc) === lc ? !1 : !0
  }, b) : c : c
};
r.A = function(a, b) {
  return new Zd(b, this.ya, this.k)
};
r.w = g("h");
r.J = function() {
  return $b(ae, this.h)
};
var ae = new Zd(null, Dd, 0);
function $d(a) {
  this.sa = a;
  this.e = 259;
  this.m = 136
}
r = $d.prototype;
r.call = function() {
  var a = null;
  return a = function(a, c, d) {
    switch(arguments.length) {
      case 2:
        return Ta.d(this.sa, c, lc) === lc ? null : c;
      case 3:
        return Ta.d(this.sa, c, lc) === lc ? d : c
    }
    throw Error("Invalid arity: " + arguments.length);
  }
}();
r.apply = function(a, b) {
  a = this;
  return a.call.apply(a, [a].concat(b.slice()))
};
r.Q = function(a, b) {
  return a.D(a, b, null)
};
r.D = function(a, b, c) {
  return Ta.d(this.sa, b, lc) === lc ? c : b
};
r.I = function() {
  return P(this.sa)
};
r.ia = function(a, b) {
  this.sa = ub(this.sa, b, null);
  return a
};
r.oa = function() {
  return new Zd(null, tb(this.sa), null)
};
function be(a) {
  a = H(a);
  if(null == a) {
    return ae
  }
  if(a instanceof Hb) {
    a = a.b;
    a: {
      for(var b = 0, c = rb(ae);;) {
        if(b < a.length) {
          var d = b + 1, c = c.ia(c, a[b]), b = d
        }else {
          a = c;
          break a
        }
      }
      a = void 0
    }
    return a.oa(a)
  }
  if(x) {
    for(d = rb(ae);;) {
      if(null != a) {
        b = a.U(a), d = d.ia(d, a.N(a)), a = b
      }else {
        return d.oa(d)
      }
    }
  }else {
    return null
  }
}
var ce = function() {
  var a = null, b = function() {
    function a(b) {
      var c = null;
      0 < arguments.length && (c = N(Array.prototype.slice.call(arguments, 0), 0));
      return be(c)
    }
    a.n = 0;
    a.l = function(a) {
      a = H(a);
      return be(a)
    };
    a.g = function(a) {
      return be(a)
    };
    return a
  }(), a = function(a) {
    switch(arguments.length) {
      case 0:
        return ae;
      default:
        return b.g(N(arguments, 0))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  a.n = 0;
  a.l = b.l;
  a.i = function() {
    return ae
  };
  a.g = b.g;
  return a
}();
function Ac(a) {
  var b;
  b = a ? ((b = a.m & 4096) ? b : a.Bc) ? !0 : !1 : !1;
  if(b) {
    return a.name
  }
  if("string" === typeof a) {
    return a
  }
  throw Error([z("Doesn't support name: "), z(a)].join(""));
}
var de = function() {
  function a(a, b) {
    for(;;) {
      var c = H(b);
      if(v(c ? 0 < a : c)) {
        var c = a - 1, h = M(b);
        a = c;
        b = h
      }else {
        return null
      }
    }
  }
  function b(a) {
    for(;;) {
      if(H(a)) {
        a = M(a)
      }else {
        return null
      }
    }
  }
  var c = null, c = function(c, e) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 2:
        return a.call(this, c, e)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.a = a;
  return c
}(), ee = function() {
  function a(a, b) {
    de.a(a, b);
    return b
  }
  function b(a) {
    de.c(a);
    return a
  }
  var c = null, c = function(c, e) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 2:
        return a.call(this, c, e)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.a = a;
  return c
}();
function X(a, b, c, d, e, f, h) {
  D(a, c);
  H(h) && (b.d ? b.d(I(h), a, f) : b.call(null, I(h), a, f));
  c = H(M(h));
  h = null;
  for(var k = 0, l = 0;;) {
    if(l < k) {
      var m = h.K(h, l);
      D(a, d);
      b.d ? b.d(m, a, f) : b.call(null, m, a, f);
      l += 1
    }else {
      if(c = H(c)) {
        h = c, ic(h) ? (c = xb(h), l = yb(h), h = c, k = P(c), c = l) : (c = I(h), D(a, d), b.d ? b.d(c, a, f) : b.call(null, c, a, f), c = M(h), h = null, k = 0), l = 0
      }else {
        break
      }
    }
  }
  return D(a, e)
}
var fe = function() {
  function a(a, d) {
    var e = null;
    1 < arguments.length && (e = N(Array.prototype.slice.call(arguments, 1), 0));
    return b.call(this, a, e)
  }
  function b(a, b) {
    for(var e = H(b), f = null, h = 0, k = 0;;) {
      if(k < h) {
        var l = f.K(f, k);
        D(a, l);
        k += 1
      }else {
        if(e = H(e)) {
          f = e, ic(f) ? (e = xb(f), h = yb(f), f = e, l = P(e), e = h, h = l) : (l = I(f), D(a, l), e = M(f), f = null, h = 0), k = 0
        }else {
          return null
        }
      }
    }
  }
  a.n = 1;
  a.l = function(a) {
    var d = I(a);
    a = J(a);
    return b(d, a)
  };
  a.g = b;
  return a
}(), ge = {'"':'\\"', "\\":"\\\\", "\b":"\\b", "\f":"\\f", "\n":"\\n", "\r":"\\r", "\t":"\\t"};
function he(a) {
  return[z('"'), z(a.replace(RegExp('[\\\\"\b\f\n\r\t]', "g"), function(a) {
    return ge[a]
  })), z('"')].join("")
}
var Y = function ie(b, c, d) {
  if(null == b) {
    return D(c, "nil")
  }
  if(void 0 === b) {
    return D(c, "#\x3cundefined\x3e")
  }
  if(x) {
    v(function() {
      var c = Tb.a(d, Eb);
      return v(c) ? (c = b ? ((c = b.e & 131072) ? c : b.Mb) ? !0 : b.e ? !1 : w(db, b) : w(db, b), v(c) ? ac(b) : c) : c
    }()) && (D(c, "^"), ie(ac(b), c, d), D(c, " "));
    if(null == b) {
      return D(c, "nil")
    }
    if(b.Da) {
      return b.Ka(b, c, d)
    }
    if(function() {
      var c;
      c = b ? ((c = b.e & 2147483648) ? c : b.L) ? !0 : !1 : !1;
      return c
    }()) {
      return b.t(b, c, d)
    }
    if(function() {
      var c = Ga(b) === Boolean;
      return c ? c : "number" === typeof b
    }()) {
      return D(c, "" + z(b))
    }
    if(b instanceof Array) {
      return X(c, ie, "#\x3cArray [", ", ", "]\x3e", d, b)
    }
    if(da(b)) {
      return v(Db.call(null, d)) ? D(c, he(b)) : D(c, b)
    }
    if(Xb(b)) {
      return fe.g(c, N(["#\x3c", "" + z(b), "\x3e"], 0))
    }
    if(b instanceof Date) {
      var e = function(b, c) {
        for(var d = "" + z(b);;) {
          if(P(d) < c) {
            d = [z("0"), z(d)].join("")
          }else {
            return d
          }
        }
      };
      return fe.g(c, N(['#inst "', "" + z(b.getUTCFullYear()), "-", e(b.getUTCMonth() + 1, 2), "-", e(b.getUTCDate(), 2), "T", e(b.getUTCHours(), 2), ":", e(b.getUTCMinutes(), 2), ":", e(b.getUTCSeconds(), 2), ".", e(b.getUTCMilliseconds(), 3), "-", '00:00"'], 0))
    }
    return v(b instanceof RegExp) ? fe.g(c, N(['#"', b.source, '"'], 0)) : function() {
      var c;
      c = b ? ((c = b.e & 2147483648) ? c : b.L) ? !0 : b.e ? !1 : w(pb, b) : w(pb, b);
      return c
    }() ? qb(b, c, d) : x ? fe.g(c, N(["#\x3c", "" + z(b), "\x3e"], 0)) : null
  }
  return null
}, je = function() {
  function a(a) {
    var d = null;
    0 < arguments.length && (d = N(Array.prototype.slice.call(arguments, 0), 0));
    return b.call(this, d)
  }
  function b(a) {
    var b = Bb([Cb, !0, Db, !0, Eb, !1, Fb, !1]), e = null == a;
    e || (e = H(a), e = v(e) ? !1 : !0);
    if(e) {
      b = ""
    }else {
      var e = z, f = new Ea, h = new zb(f);
      a: {
        Y(I(a), h, b);
        a = H(M(a));
        for(var k = null, l = 0, m = 0;;) {
          if(m < l) {
            var n = k.K(k, m);
            D(h, " ");
            Y(n, h, b);
            m += 1
          }else {
            if(a = H(a)) {
              k = a, ic(k) ? (a = xb(k), l = yb(k), k = a, n = P(a), a = l, l = n) : (n = I(k), D(h, " "), Y(n, h, b), a = M(k), k = null, l = 0), m = 0
            }else {
              break a
            }
          }
        }
      }
      ob(h);
      b = "" + e(f)
    }
    return b
  }
  a.n = 0;
  a.l = function(a) {
    a = H(a);
    return b(a)
  };
  a.g = b;
  return a
}();
Xd.prototype.L = !0;
Xd.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
Hb.prototype.L = !0;
Hb.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
od.prototype.L = !0;
od.prototype.t = function(a, b, c) {
  return X(b, Y, "[", " ", "]", c, a)
};
Hc.prototype.L = !0;
Hc.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
zd.prototype.L = !0;
zd.prototype.t = function(a, b, c) {
  return X(b, function(a) {
    return X(b, Y, "", " ", "", c, a)
  }, "{", ", ", "}", c, a)
};
Cc.prototype.L = !0;
Cc.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
Rd.prototype.L = !0;
Rd.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
md.prototype.L = !0;
md.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
Td.prototype.L = !0;
Td.prototype.t = function(a, b, c) {
  return X(b, function(a) {
    return X(b, Y, "", " ", "", c, a)
  }, "{", ", ", "}", c, a)
};
Zd.prototype.L = !0;
Zd.prototype.t = function(a, b, c) {
  return X(b, Y, "#{", " ", "}", c, a)
};
fd.prototype.L = !0;
fd.prototype.t = function(a, b, c) {
  return X(b, Y, "[", " ", "]", c, a)
};
wc.prototype.L = !0;
wc.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
yd.prototype.L = !0;
yd.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
xc.prototype.L = !0;
xc.prototype.t = function(a, b) {
  return D(b, "()")
};
yc.prototype.L = !0;
yc.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
Sd.prototype.L = !0;
Sd.prototype.t = function(a, b, c) {
  return X(b, Y, "(", " ", ")", c, a)
};
fd.prototype.pb = !0;
fd.prototype.qb = function(a, b) {
  return pc.a(a, b)
};
od.prototype.pb = !0;
od.prototype.qb = function(a, b) {
  return pc.a(a, b)
};
function ke(a, b, c, d) {
  this.state = a;
  this.h = b;
  this.Tc = c;
  this.Uc = d;
  this.e = 2153938944;
  this.m = 2
}
r = ke.prototype;
r.C = function(a) {
  return a[fa] || (a[fa] = ++ga)
};
r.t = function(a, b, c) {
  D(b, "#\x3cAtom: ");
  Y(this.state, b, c);
  return D(b, "\x3e")
};
r.w = g("h");
r.Ta = g("state");
r.u = function(a, b) {
  return a === b
};
var me = function() {
  function a(a) {
    return new ke(a, null, null, null)
  }
  var b = null, c = function() {
    function a(c, d) {
      var k = null;
      1 < arguments.length && (k = N(Array.prototype.slice.call(arguments, 1), 0));
      return b.call(this, c, k)
    }
    function b(a, c) {
      var d = mc(c) ? Zb.a(Ub, c) : c, e = Tb.a(d, le), d = Tb.a(d, Eb);
      return new ke(a, d, e, null)
    }
    a.n = 1;
    a.l = function(a) {
      var c = I(a);
      a = J(a);
      return b(c, a)
    };
    a.g = b;
    return a
  }(), b = function(b, e) {
    switch(arguments.length) {
      case 1:
        return a.call(this, b);
      default:
        return c.g(b, N(arguments, 1))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  b.n = 1;
  b.l = c.l;
  b.c = a;
  b.g = c.g;
  return b
}(), ne = {};
function oe(a, b) {
  if(a ? a.Kb : a) {
    return a.Kb(a, b)
  }
  var c;
  c = oe[u(null == a ? null : a)];
  if(!c && (c = oe._, !c)) {
    throw y("IEncodeClojure.-js-\x3eclj", a);
  }
  return c.call(null, a, b)
}
var qe = function() {
  function a(a) {
    return b.g(a, N([Bb([pe, !1])], 0))
  }
  var b = null, c = function() {
    function a(c, d) {
      var k = null;
      1 < arguments.length && (k = N(Array.prototype.slice.call(arguments, 1), 0));
      return b.call(this, c, k)
    }
    function b(a, c) {
      if(a ? v(v(null) ? null : a.zc) || (a.Pb ? 0 : w(ne, a)) : w(ne, a)) {
        return oe(a, Zb.a(Wd, c))
      }
      if(H(c)) {
        var d = mc(c) ? Zb.a(Ub, c) : c, e = Tb.a(d, pe);
        return function(a, b, c, d) {
          return function E(e) {
            return mc(e) ? ee.c(Sc.a(E, e)) : dc(e) ? Wc(Ma(e), Sc.a(E, e)) : e instanceof Array ? ld(Sc.a(E, e)) : Ga(e) === Object ? Wc(Dd, function() {
              return function(a, b, c, d) {
                return function vb(f) {
                  return new Cc(null, function(a, b, c, d) {
                    return function() {
                      for(;;) {
                        var a = H(f);
                        if(a) {
                          if(ic(a)) {
                            var b = xb(a), c = P(b), h = new Ec(Array(c), 0);
                            a: {
                              for(var k = 0;;) {
                                if(k < c) {
                                  var l = A.a(b, k), l = W([d.c ? d.c(l) : d.call(null, l), E(e[l])]);
                                  h.add(l);
                                  k += 1
                                }else {
                                  b = !0;
                                  break a
                                }
                              }
                              b = void 0
                            }
                            return b ? Ic(h.Z(), vb(yb(a))) : Ic(h.Z(), null)
                          }
                          h = I(a);
                          return O(W([d.c ? d.c(h) : d.call(null, h), E(e[h])]), vb(J(a)))
                        }
                        return null
                      }
                    }
                  }(a, b, c, d), null, null)
                }
              }(a, b, c, d)(jc(e))
            }()) : x ? e : null
          }
        }(c, d, e, v(e) ? Bc : z)(a)
      }
      return null
    }
    a.n = 1;
    a.l = function(a) {
      var c = I(a);
      a = J(a);
      return b(c, a)
    };
    a.g = b;
    return a
  }(), b = function(b, e) {
    switch(arguments.length) {
      case 1:
        return a.call(this, b);
      default:
        return c.g(b, N(arguments, 1))
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  b.n = 1;
  b.l = c.l;
  b.c = a;
  b.g = c.g;
  return b
}();
var Fb = new T(null, "dup", "dup"), re = new T(null, "life-range", "life-range"), $ = new T(null, "recur", "recur"), se = new T(null, "data", "data"), te = new T(null, "start-n", "start-n"), ue = new T(null, "n-per-sec", "n-per-sec"), pe = new T(null, "keywordize-keys", "keywordize-keys"), Cb = new T(null, "flush-on-newline", "flush-on-newline"), ve = new T(null, "pos", "pos"), we = new T(null, "vel", "vel"), xe = new T(null, "channels", "channels"), x = new T(null, "else", "else"), Db = new T(null, 
"readably", "readably"), le = new T(null, "validator", "validator"), Eb = new T(null, "meta", "meta"), ye = new T(null, "life", "life"), ze = new T(null, "spawn?", "spawn?");
function Ae(a, b) {
  var c = R.d(a, 0, null), d = R.d(a, 1, null), e = R.d(b, 0, null), f = R.d(b, 1, null);
  return W([c + e, d + f])
}
function Be(a, b) {
  var c = R.d(a, 0, null), d = R.d(a, 1, null);
  return W([c * b, d * b])
}
;function Ce() {
  0 != De && (this.Hc = Error().stack, this[fa] || (this[fa] = ++ga))
}
var De = 0;
var Ee, Fe, Ge, He;
function Ie() {
  return ba.navigator ? ba.navigator.userAgent : null
}
He = Ge = Fe = Ee = !1;
var Je;
if(Je = Ie()) {
  var Ke = ba.navigator;
  Ee = 0 == Je.indexOf("Opera");
  Fe = !Ee && -1 != Je.indexOf("MSIE");
  Ge = !Ee && -1 != Je.indexOf("WebKit");
  He = !Ee && !Ge && "Gecko" == Ke.product
}
var Le = Ee, Me = Fe, Ne = He, Oe = Ge;
function Pe() {
  var a = ba.document;
  return a ? a.documentMode : void 0
}
var Qe;
a: {
  var Re = "", Se;
  if(Le && ba.opera) {
    var Te = ba.opera.version, Re = "function" == typeof Te ? Te() : Te
  }else {
    if(Ne ? Se = /rv\:([^\);]+)(\)|;)/ : Me ? Se = /MSIE\s+([^\);]+)(\)|;)/ : Oe && (Se = /WebKit\/(\S+)/), Se) {
      var Ue = Se.exec(Ie()), Re = Ue ? Ue[1] : ""
    }
  }
  if(Me) {
    var Ve = Pe();
    if(Ve > parseFloat(Re)) {
      Qe = String(Ve);
      break a
    }
  }
  Qe = Re
}
var We = {};
function Xe(a) {
  var b;
  if(!(b = We[a])) {
    b = 0;
    for(var c = String(Qe).replace(/^[\s\xa0]+|[\s\xa0]+$/g, "").split("."), d = String(a).replace(/^[\s\xa0]+|[\s\xa0]+$/g, "").split("."), e = Math.max(c.length, d.length), f = 0;0 == b && f < e;f++) {
      var h = c[f] || "", k = d[f] || "", l = RegExp("(\\d*)(\\D*)", "g"), m = RegExp("(\\d*)(\\D*)", "g");
      do {
        var n = l.exec(h) || ["", "", ""], q = m.exec(k) || ["", "", ""];
        if(0 == n[0].length && 0 == q[0].length) {
          break
        }
        b = ((0 == n[1].length ? 0 : parseInt(n[1], 10)) < (0 == q[1].length ? 0 : parseInt(q[1], 10)) ? -1 : (0 == n[1].length ? 0 : parseInt(n[1], 10)) > (0 == q[1].length ? 0 : parseInt(q[1], 10)) ? 1 : 0) || ((0 == n[2].length) < (0 == q[2].length) ? -1 : (0 == n[2].length) > (0 == q[2].length) ? 1 : 0) || (n[2] < q[2] ? -1 : n[2] > q[2] ? 1 : 0)
      }while(0 == b)
    }
    b = We[a] = 0 <= b
  }
  return b
}
var Ye = ba.document, Ze = Ye && Me ? Pe() || ("CSS1Compat" == Ye.compatMode ? parseInt(Qe, 10) : 5) : void 0;
Me && Xe("9");
!Oe || Xe("528");
Ne && Xe("1.9b") || Me && Xe("8") || Le && Xe("9.5") || Oe && Xe("528");
Ne && !Xe("8") || Me && Xe("9");
function $e(a) {
  return af(a || arguments.callee.caller, [])
}
function af(a, b) {
  var c = [];
  if(0 <= za(b, a)) {
    c.push("[...circular reference...]")
  }else {
    if(a && 50 > b.length) {
      c.push(bf(a) + "(");
      for(var d = a.arguments, e = 0;e < d.length;e++) {
        0 < e && c.push(", ");
        var f;
        f = d[e];
        switch(typeof f) {
          case "object":
            f = f ? "object" : "null";
            break;
          case "string":
            break;
          case "number":
            f = String(f);
            break;
          case "boolean":
            f = f ? "true" : "false";
            break;
          case "function":
            f = (f = bf(f)) ? f : "[fn]";
            break;
          default:
            f = typeof f
        }
        40 < f.length && (f = f.substr(0, 40) + "...");
        c.push(f)
      }
      b.push(a);
      c.push(")\n");
      try {
        c.push(af(a.caller, b))
      }catch(h) {
        c.push("[exception trying to get caller]\n")
      }
    }else {
      a ? c.push("[...long stack...]") : c.push("[end]")
    }
  }
  return c.join("")
}
function bf(a) {
  if(cf[a]) {
    return cf[a]
  }
  a = String(a);
  if(!cf[a]) {
    var b = /function ([^\(]+)/.exec(a);
    cf[a] = b ? b[1] : "[Anonymous]"
  }
  return cf[a]
}
var cf = {};
function df(a, b, c, d, e) {
  this.reset(a, b, c, d, e)
}
df.prototype.$b = 0;
df.prototype.yb = null;
df.prototype.xb = null;
var ef = 0;
df.prototype.reset = function(a, b, c, d, e) {
  this.$b = "number" == typeof e ? e : ef++;
  this.Rc = d || ka();
  this.Ga = a;
  this.Xb = b;
  this.Kc = c;
  delete this.yb;
  delete this.xb
};
df.prototype.Db = function(a) {
  this.Ga = a
};
function ff(a) {
  this.Yb = a
}
ff.prototype.Na = null;
ff.prototype.Ga = null;
ff.prototype.Qa = null;
ff.prototype.Bb = null;
function gf(a, b) {
  this.name = a;
  this.value = b
}
gf.prototype.toString = g("name");
var hf = new gf("INFO", 800), jf = new gf("CONFIG", 700);
r = ff.prototype;
r.getParent = g("Na");
r.zb = function() {
  this.Qa || (this.Qa = {});
  return this.Qa
};
r.Db = function(a) {
  this.Ga = a
};
function kf(a) {
  if(a.Ga) {
    return a.Ga
  }
  if(a.Na) {
    return kf(a.Na)
  }
  xa("Root logger has no level set.");
  return null
}
r.log = function(a, b, c) {
  if(a.value >= kf(this).value) {
    for(a = this.Sb(a, b, c), b = "log:" + a.Xb, ba.console && (ba.console.timeStamp ? ba.console.timeStamp(b) : ba.console.markTimeline && ba.console.markTimeline(b)), ba.msWriteProfilerMark && ba.msWriteProfilerMark(b), b = this;b;) {
      c = b;
      var d = a;
      if(c.Bb) {
        for(var e = 0, f = void 0;f = c.Bb[e];e++) {
          f(d)
        }
      }
      b = b.getParent()
    }
  }
};
r.Sb = function(a, b, c) {
  var d = new df(a, String(b), this.Yb);
  if(c) {
    d.yb = c;
    var e;
    var f = arguments.callee.caller;
    try {
      var h;
      var k;
      c: {
        for(var l = ["window", "location", "href"], m = ba, n;n = l.shift();) {
          if(null != m[n]) {
            m = m[n]
          }else {
            k = null;
            break c
          }
        }
        k = m
      }
      if(da(c)) {
        h = {message:c, name:"Unknown error", lineNumber:"Not available", fileName:k, stack:"Not available"}
      }else {
        var q, s, l = !1;
        try {
          q = c.lineNumber || c.Jc || "Not available"
        }catch(t) {
          q = "Not available", l = !0
        }
        try {
          s = c.fileName || c.filename || c.sourceURL || ba.$googDebugFname || k
        }catch(E) {
          s = "Not available", l = !0
        }
        h = !l && c.lineNumber && c.fileName && c.stack ? c : {message:c.message, name:c.name, lineNumber:q, fileName:s, stack:c.stack || "Not available"}
      }
      e = "Message: " + oa(h.message) + '\nUrl: \x3ca href\x3d"view-source:' + h.fileName + '" target\x3d"_new"\x3e' + h.fileName + "\x3c/a\x3e\nLine: " + h.lineNumber + "\n\nBrowser stack:\n" + oa(h.stack + "-\x3e ") + "[end]\n\nJS stack traversal:\n" + oa($e(f) + "-\x3e ")
    }catch(Q) {
      e = "Exception trying to expose exception! You win, we lose. " + Q
    }
    d.xb = e
  }
  return d
};
r.info = function(a, b) {
  this.log(hf, a, b)
};
var lf = {}, mf = null;
function nf(a) {
  mf || (mf = new ff(""), lf[""] = mf, mf.Db(jf));
  var b;
  if(!(b = lf[a])) {
    b = new ff(a);
    var c = a.lastIndexOf("."), d = a.substr(c + 1), c = nf(a.substr(0, c));
    c.zb()[d] = b;
    b.Na = c;
    lf[a] = b
  }
  return b
}
;nf("goog.net.XhrIo");
function of(a) {
  return Math.floor.c ? Math.floor.c((Math.random.i ? Math.random.i() : Math.random.call(null)) * a) : Math.floor.call(null, (Math.random.i ? Math.random.i() : Math.random.call(null)) * a)
}
function pf(a, b) {
  var c = b < a ? b : a;
  return 0 > c ? 0 : c
}
;function qf() {
  var a = rf, b = R.d(a, 0, null), c = R.d(a, 1, null), d = R.d(a, 2, null), a = R.d(a, 3, null);
  return Rb.a(ld(Sc.a(function(a) {
    a -= 30;
    return 0 > a ? 0 : a
  }, W([b, c, d]))), a)
}
function sf(a) {
  var b = R.d(a, 0, null), c = R.d(a, 1, null), d = R.d(a, 2, null);
  a = R.d(a, 3, null);
  return[z("rgba("), z(b), z(","), z(c), z(","), z(d), z(","), z(a), z(")")].join("")
}
function tf(a, b, c) {
  var d = R.d(b, 0, null);
  b = R.d(b, 1, null);
  a.beginPath();
  a.arc(d, b, c, 0, 2 * Math.PI);
  a.fill();
  a.stroke()
}
;var uf = {dc:"cn", cc:"at", rc:"rat", nc:"pu", gc:"ifrid", uc:"tp", ic:"lru", mc:"pru", Eb:"lpu", Fb:"ppu", lc:"ph", kc:"osh", sc:"role", jc:"nativeProtocolVersion"}, vf = nf("goog.net.xpc");
/*
 Portions of this code are from MochiKit, received by
 The Closure Authors under the MIT license. All other code is Copyright
 2005-2009 The Closure Authors. All Rights Reserved.
*/
var wf;
function xf(a, b) {
  if(a ? a.kb : a) {
    return a.kb(0, b)
  }
  var c;
  c = xf[u(null == a ? null : a)];
  if(!c && (c = xf._, !c)) {
    throw y("ReadPort.take!", a);
  }
  return c.call(null, a, b)
}
function yf(a) {
  if(a ? a.Ja : a) {
    return a.Ja()
  }
  var b;
  b = yf[u(null == a ? null : a)];
  if(!b && (b = yf._, !b)) {
    throw y("Channel.close!", a);
  }
  return b.call(null, a)
}
function zf(a) {
  if(a ? a.vb : a) {
    return!0
  }
  var b;
  b = zf[u(null == a ? null : a)];
  if(!b && (b = zf._, !b)) {
    throw y("Handler.active?", a);
  }
  return b.call(null, a)
}
;function Af(a, b, c, d, e) {
  for(var f = 0;;) {
    if(f < e) {
      c[d + f] = a[b + f], f += 1
    }else {
      break
    }
  }
}
function Bf(a, b, c, d) {
  this.head = a;
  this.q = b;
  this.length = c;
  this.b = d
}
Bf.prototype.pop = function() {
  if(0 === this.length) {
    return null
  }
  var a = this.b[this.q];
  this.b[this.q] = null;
  this.q = (this.q + 1) % this.b.length;
  this.length -= 1;
  return a
};
Bf.prototype.unshift = function(a) {
  this.b[this.head] = a;
  this.head = (this.head + 1) % this.b.length;
  this.length += 1;
  return null
};
function Cf(a, b) {
  if(a.length + 1 === a.b.length) {
    var c = Array(2 * a.b.length);
    a.q < a.head ? (Af(a.b, a.q, c, 0, a.length), a.q = 0, a.head = a.length, a.b = c) : a.q > a.head ? (Af(a.b, a.q, c, 0, a.b.length - a.q), Af(a.b, 0, c, a.b.length - a.q, a.head), a.q = 0, a.head = a.length, a.b = c) : a.q === a.head && (a.q = 0, a.head = 0, a.b = c)
  }
  a.unshift(b)
}
function Df(a, b) {
  for(var c = a.length, d = 0;;) {
    if(d < c) {
      var e = a.pop();
      (b.c ? b.c(e) : b.call(null, e)) && a.unshift(e);
      d += 1
    }else {
      break
    }
  }
}
function Ef(a) {
  if(!(0 < a)) {
    throw Error([z("Assert failed: "), z("Can't create a ring buffer of size 0"), z("\n"), z(je.g(N([Ob(new F(null, "\x3e", "\x3e", -1640531465, null), new F(null, "n", "n", -1640531417, null), 0)], 0)))].join(""));
  }
  return new Bf(0, 0, 0, Array(a))
}
function Ff(a, b) {
  this.H = a;
  this.Cb = b;
  this.m = 0;
  this.e = 2
}
Ff.prototype.I = function() {
  return this.H.length
};
function Gf(a, b, c) {
  if(v(b.H.length === b.Cb)) {
    throw Error([z("Assert failed: "), z("Can't add to a full buffer"), z("\n"), z(je.g(N([Ob(new F(null, "not", "not", -1640422260, null), Ob(new F("impl", "full?", "impl/full?", -1337857039, null), new F(null, "this", "this", -1636972457, null)))], 0)))].join(""));
  }
  a.H.unshift(c)
}
;var Hf = null, If = Ef(32), Jf = !1, Kf = !1;
function Lf() {
  Jf = !0;
  Kf = !1;
  for(var a = 0;;) {
    var b = If.pop();
    if(null != b && (b.i ? b.i() : b.call(null), 1024 > a)) {
      a += 1;
      continue
    }
    break
  }
  Jf = !1;
  return 0 < If.length ? Mf.i ? Mf.i() : Mf.call(null) : null
}
"undefined" !== typeof MessageChannel && (Hf = new MessageChannel, Hf.port1.onmessage = function() {
  return Lf()
});
function Mf() {
  var a = Kf;
  if(v(v(a) ? Jf : a)) {
    return null
  }
  Kf = !0;
  return"undefined" !== typeof MessageChannel ? Hf.port2.postMessage(0) : "undefined" !== typeof setImmediate ? setImmediate(Lf) : x ? setTimeout(Lf, 0) : null
}
function Nf(a) {
  Cf(If, a);
  Mf()
}
;var Of, Pf = !Ne && !Me || Me && Me && 9 <= Ze || Ne && Xe("1.9.1");
Me && Xe("9");
function Qf(a, b, c) {
  function d(c) {
    c && b.appendChild(da(c) ? a.createTextNode(c) : c)
  }
  for(var e = 1;e < c.length;e++) {
    var f = c[e];
    !ca(f) || ea(f) && 0 < f.nodeType ? d(f) : Aa(Rf(f) ? Ca(f) : f, d)
  }
}
function Rf(a) {
  if(a && "number" == typeof a.length) {
    if(ea(a)) {
      return"function" == typeof a.item || "string" == typeof a.item
    }
    if("function" == u(a)) {
      return"function" == typeof a.item
    }
  }
  return!1
}
function Sf(a) {
  this.Rb = a || ba.document || document
}
Sf.prototype.createTextNode = function(a) {
  return this.Rb.createTextNode(String(a))
};
Sf.prototype.appendChild = function(a, b) {
  a.appendChild(b)
};
Sf.prototype.append = function(a, b) {
  Qf(9 == a.nodeType ? a : a.ownerDocument || a.document, a, arguments)
};
Sf.prototype.zb = function(a) {
  return Pf && void 0 != a.children ? a.children : Ba(a.childNodes, function(a) {
    return 1 == a.nodeType
  })
};
function Tf(a) {
  Ce.call(this);
  this.Ic = a || Of || (Of = new Sf)
}
la(Tf, Ce);
a: {
  for(var Uf = document.getElementById("my-canvas"), Vf = Uf.getContext("2d"), Wf = Uf.width, Yf = Uf.height, Zf = 0;;) {
    if(30 > Zf) {
      var rf = W([of(255), of(255), of(255), Math.random.i ? Math.random.i() : Math.random.call(null)]), $f = qf(), ag = of(20), bg, cg = Yf;
      bg = W([of(Wf), of(cg)]);
      var dg = Vf, eg = $f;
      dg.fillStyle = sf(rf);
      dg.strokeStyle = sf(eg);
      dg.lineWidth = 5;
      tf(Vf, bg, ag);
      Zf += 1
    }else {
      break a
    }
  }
}
;var fg, hg = function gg(b) {
  "undefined" === typeof fg && (fg = {}, fg = function(b, d, e) {
    this.ka = b;
    this.lb = d;
    this.Wb = e;
    this.m = 0;
    this.e = 393216
  }, fg.Da = !0, fg.Ca = "cljs.core.async.impl.ioc-helpers/t20069", fg.Ka = function(b, d) {
    return D(d, "cljs.core.async.impl.ioc-helpers/t20069")
  }, fg.prototype.vb = p(!0), fg.prototype.w = g("Wb"), fg.prototype.A = function(b, d) {
    return new fg(this.ka, this.lb, d)
  });
  return new fg(b, gg, null)
};
function ig(a) {
  try {
    return a[0].call(null, a)
  }catch(b) {
    if(b instanceof Object) {
      throw a[4].Ja(), b;
    }
    if(x) {
      throw b;
    }
    return null
  }
}
function jg(a, b) {
  var c = b.kb(0, hg(function(b) {
    a[2] = b;
    a[1] = 9;
    return ig(a)
  }));
  return v(c) ? (a[2] = cb(c), a[1] = 9, $) : null
}
function kg(a, b, c, d) {
  c = c.wb(d, hg(function() {
    a[2] = null;
    a[1] = b;
    return ig(a)
  }));
  return v(c) ? (a[2] = cb(c), a[1] = b, $) : null
}
function lg(a, b) {
  var c = a[4];
  null != b && c.wb(b, hg(p(null)));
  c.Ja();
  return c
}
;var mg, og = function ng(b) {
  "undefined" === typeof mg && (mg = {}, mg = function(b, d, e) {
    this.M = b;
    this.Gb = d;
    this.Vb = e;
    this.m = 0;
    this.e = 425984
  }, mg.Da = !0, mg.Ca = "cljs.core.async.impl.channels/t20058", mg.Ka = function(b, d) {
    return D(d, "cljs.core.async.impl.channels/t20058")
  }, mg.prototype.Ta = g("M"), mg.prototype.w = g("Vb"), mg.prototype.A = function(b, d) {
    return new mg(this.M, this.Gb, d)
  });
  return new mg(b, ng, null)
};
function pg(a, b) {
  this.Ab = a;
  this.M = b
}
function qg(a) {
  return zf(a.Ab)
}
function rg(a, b, c, d, e, f) {
  this.Ha = a;
  this.Ma = b;
  this.Oa = c;
  this.La = d;
  this.H = e;
  this.closed = f
}
rg.prototype.Ja = function() {
  if(!this.closed) {
    for(this.closed = !0;;) {
      var a = this.Ha.pop();
      if(null != a) {
        Nf(function(a) {
          return function() {
            return a.c ? a.c(null) : a.call(null, null)
          }
        }(a.ka, a))
      }else {
        break
      }
    }
  }
  return null
};
rg.prototype.kb = function(a, b) {
  var c = null != this.H;
  if(c ? 0 < P(this.H) : c) {
    return og(this.H.H.pop())
  }
  for(;;) {
    c = this.Oa.pop();
    if(null != c) {
      var d = c.M;
      Nf(c.Ab.ka);
      return og(d)
    }
    if(this.closed) {
      return og(null)
    }
    64 < this.Ma ? (this.Ma = 0, Df(this.Ha, zf)) : this.Ma += 1;
    if(!(1024 > this.Ha.length)) {
      throw Error([z("Assert failed: "), z([z("No more than "), z(1024), z(" pending takes are allowed on a single channel.")].join("")), z("\n"), z(je.g(N([Ob(new F(null, "\x3c", "\x3c", -1640531467, null), Ob(new F(null, ".-length", ".-length", 1395928862, null), new F(null, "takes", "takes", -1530407291, null)), new F("impl", "MAX-QUEUE-SIZE", "impl/MAX-QUEUE-SIZE", -1989946393, null))], 0)))].join(""));
    }
    Cf(this.Ha, b);
    return null
  }
};
rg.prototype.wb = function(a, b) {
  var c = this;
  if(null == a) {
    throw Error([z("Assert failed: "), z("Can't put nil in on a channel"), z("\n"), z(je.g(N([Ob(new F(null, "not", "not", -1640422260, null), Ob(new F(null, "nil?", "nil?", -1637150201, null), new F(null, "val", "val", -1640415014, null)))], 0)))].join(""));
  }
  var d = c.closed;
  if(d && d) {
    return og(null)
  }
  for(;;) {
    d = c.Ha.pop();
    if(null != d) {
      var e = b.ka;
      Nf(function(b) {
        return function() {
          return b.c ? b.c(a) : b.call(null, a)
        }
      }(d.ka, e, d))
    }else {
      if(function() {
        var a = null == c.H;
        return a ? a : c.H.H.length === c.H.Cb
      }()) {
        64 < c.La ? (c.La = 0, Df(c.Oa, qg)) : c.La += 1;
        if(!(1024 > c.Oa.length)) {
          throw Error([z("Assert failed: "), z([z("No more than "), z(1024), z(" pending puts are allowed on a single channel."), z(" Consider using a windowed buffer.")].join("")), z("\n"), z(je.g(N([Ob(new F(null, "\x3c", "\x3c", -1640531467, null), Ob(new F(null, ".-length", ".-length", 1395928862, null), new F(null, "puts", "puts", -1637078787, null)), new F("impl", "MAX-QUEUE-SIZE", "impl/MAX-QUEUE-SIZE", -1989946393, null))], 0)))].join(""));
        }
        Cf(c.Oa, new pg(b, a));
        return null
      }
      e = b.ka;
      Gf(c.H, c.H, a)
    }
    return og(null)
  }
};
function sg(a, b, c) {
  this.key = a;
  this.M = b;
  this.forward = c;
  this.m = 0;
  this.e = 2155872256
}
sg.prototype.t = function(a, b, c) {
  return X(b, Y, "[", " ", "]", c, a)
};
sg.prototype.v = function() {
  return Ob.g(N([this.key, this.M], 0))
};
(function() {
  function a(a, b, c) {
    c = Array(c + 1);
    for(var h = 0;;) {
      if(h < c.length) {
        c[h] = null, h += 1
      }else {
        break
      }
    }
    return new sg(a, b, c)
  }
  function b(a) {
    return c.d(null, null, a)
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 1:
        return b.call(this, c);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.c = b;
  c.d = a;
  return c
})().c(0);
var ug = function tg(b) {
  "undefined" === typeof wf && (wf = {}, wf = function(b, d, e) {
    this.ka = b;
    this.lb = d;
    this.Ub = e;
    this.m = 0;
    this.e = 393216
  }, wf.Da = !0, wf.Ca = "cljs.core.async/t18088", wf.Ka = function(b, d) {
    return D(d, "cljs.core.async/t18088")
  }, wf.prototype.vb = p(!0), wf.prototype.w = g("Ub"), wf.prototype.A = function(b, d) {
    return new wf(this.ka, this.lb, d)
  });
  return new wf(b, tg, null)
}, vg = function() {
  function a(a) {
    a = Ib.a(a, 0) ? null : a;
    a = "number" === typeof a ? new Ff(Ef(a), a) : a;
    return new rg(Ef(32), 0, Ef(32), 0, a, null)
  }
  function b() {
    return c.c(null)
  }
  var c = null, c = function(c) {
    switch(arguments.length) {
      case 0:
        return b.call(this);
      case 1:
        return a.call(this, c)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.i = b;
  c.c = a;
  return c
}(), wg = function() {
  function a(a, b, c) {
    a = xf(a, ug(b));
    if(v(a)) {
      var h = cb(a);
      v(c) ? b.c ? b.c(h) : b.call(null, h) : Nf(function() {
        return b.c ? b.c(h) : b.call(null, h)
      })
    }
    return null
  }
  function b(a, b) {
    return c.d(a, b, !0)
  }
  var c = null, c = function(c, e, f) {
    switch(arguments.length) {
      case 2:
        return b.call(this, c, e);
      case 3:
        return a.call(this, c, e, f)
    }
    throw Error("Invalid arity: " + arguments.length);
  };
  c.a = b;
  c.d = a;
  return c
}();
nf("goog.messaging.AbstractChannel");
function xg(a, b) {
  Tf.call(this, b);
  this.Pa = a;
  this.Pc = this.Pa.Hb[uf.Fb];
  this.Nc = this.Pa.Hb[uf.Eb];
  this.Oc = []
}
var yg;
la(xg, Tf);
var zg = [], Ag = ja(function() {
  var a, b = !1;
  try {
    for(var c = 0;a = zg[c];c++) {
      var d;
      if(!(d = b)) {
        var e = a, f = e.Mc.location.href;
        if(f != e.Qb) {
          e.Qb = f;
          var h = f.split("#")[1];
          h && (h = h.substr(1), e.vc(decodeURIComponent(h)));
          d = !0
        }else {
          d = !1
        }
      }
      b = d
    }
  }catch(k) {
    if(vf.info("receive_() failed: " + k), a = a.Sc.Pa, vf.info("Transport Error"), a.close(), !zg.length) {
      return
    }
  }
  a = ka();
  b && (yg = a);
  window.setTimeout(Ag, 1E3 > a - yg ? 10 : 100)
}, xg);
Wc(Dd, Sc.a(function(a) {
  var b = R.d(a, 0, null);
  a = R.d(a, 1, null);
  return W([Bc.c(b.toLowerCase()), a])
}, Yd.g(N([qe.c({ec:"complete", tc:"success", fc:"error", bc:"abort", pc:"ready", qc:"readystatechange", TIMEOUT:"timeout", hc:"incrementaldata", oc:"progress"})], 0))));
Wc(Dd, Sc.a(function(a) {
  var b = R.d(a, 0, null);
  a = R.d(a, 1, null);
  return W([Bc.c(b.toLowerCase()), a])
}, qe.c(uf)));
me.c(null);
me.c(0);
var Bg = function() {
  function a(a, d, e, f, h) {
    var k = null;
    4 < arguments.length && (k = N(Array.prototype.slice.call(arguments, 4), 0));
    return b.call(this, a, d, e, f, k)
  }
  function b(a, b, e, f, h) {
    var k = vg.i(), l = Date.now.i ? Date.now.i() : Date.now.call(null), m = l + e, n = vg.c(1);
    Nf(function() {
      var q = function() {
        return function(a) {
          return function() {
            function b(c) {
              for(;;) {
                var d = a(c);
                if(!zc(d, $)) {
                  return d
                }
              }
            }
            function c() {
              var a = Array(13);
              a[0] = d;
              a[1] = 1;
              return a
            }
            var d = null, d = function(a) {
              switch(arguments.length) {
                case 0:
                  return c.call(this);
                case 1:
                  return b.call(this, a)
              }
              throw Error("Invalid arity: " + arguments.length);
            };
            d.i = c;
            d.c = b;
            return d
          }()
        }(function(t) {
          var s = t[1];
          if(7 === s) {
            var n = t[6], s = t[2], q = t[7], K = t[5];
            t[8] = q;
            t[9] = K;
            t[10] = n;
            t[11] = s;
            t[2] = null;
            t[1] = 2;
            return $
          }
          if(6 === s) {
            return s = t[2], t[2] = s, t[1] = 3, $
          }
          if(5 === s) {
            return n = t[6], s = t[12], s = Ub.g(N([ve, n, ye, s], 0)), s = Wc(s, h), kg(t, 7, k, s)
          }
          if(4 === s) {
            return s = yf(k), t[2] = s, t[1] = 6, $
          }
          if(3 === s) {
            return s = t[2], lg(t, s)
          }
          if(2 === s) {
            var q = t[8], K = t[9], n = t[10], U = Date.now.i ? Date.now.i() : Date.now.call(null), s = 1 - (U - l) / e, K = (U - K) / 1E3, ma = f.a ? f.a(a, U) : f.call(null, a, U), ma = Be(ma, K), q = Ae(q, ma), K = Be(q, K), n = Ae(n, K), K = U >= m;
            t[5] = U;
            t[6] = n;
            t[7] = q;
            t[12] = s;
            t[1] = v(K) ? 4 : 5;
            return $
          }
          return 1 === s ? (s = Date.now.i ? Date.now.i() : Date.now.call(null), n = a, q = b, t[8] = q, t[9] = s, t[10] = n, t[2] = null, t[1] = 2, $) : null
        })
      }(), s = function() {
        var a = q.i ? q.i() : q.call(null);
        a[4] = n;
        return a
      }();
      return ig(s)
    });
    return k
  }
  a.n = 4;
  a.l = function(a) {
    var d = I(a);
    a = M(a);
    var e = I(a);
    a = M(a);
    var f = I(a);
    a = M(a);
    var h = I(a);
    a = J(a);
    return b(d, e, f, h, a)
  };
  a.g = b;
  return a
}();
function Cg(a, b) {
  var c = Math.floor.c ? Math.floor.c((Math.random.i ? Math.random.i() : Math.random.call(null)) * b) : Math.floor.call(null, (Math.random.i ? Math.random.i() : Math.random.call(null)) * b);
  return a > c ? a : c
}
var Dg = function() {
  function a(a, d, e) {
    var f = null;
    2 < arguments.length && (f = N(Array.prototype.slice.call(arguments, 2), 0));
    return b.call(this, a, d, f)
  }
  function b(a, b, e) {
    var f = mc(e) ? Zb.a(Ub, e) : e;
    e = Tb.a(f, re);
    var h = Tb.a(f, we);
    Tb.a(f, ue);
    Tb.a(f, ze);
    var f = Tb.a(f, te), k = vg.i(), f = v(f) ? f : 20, h = v(h) ? h : 100;
    e = v(e) ? e : W([1E3, 1E4]);
    var l = Wc(jd, Vc.a(f, function(e, f, h, k) {
      return function() {
        return Bg(a, W([2 * ((Math.random.i ? Math.random.i() : Math.random.call(null)) - 0.5) * h, 2 * ((Math.random.i ? Math.random.i() : Math.random.call(null)) - 0.5) * h]), Zb.a(Cg, k), b)
      }
    }(k, f, h, e))), m = vg.c(1);
    Nf(function() {
      var a = function() {
        return function(a) {
          return function() {
            function b(c) {
              for(;;) {
                var d = a(c);
                if(!zc(d, $)) {
                  return d
                }
              }
            }
            function c() {
              var a = Array(14);
              a[0] = d;
              a[1] = 1;
              return a
            }
            var d = null, d = function(a) {
              switch(arguments.length) {
                case 0:
                  return c.call(this);
                case 1:
                  return b.call(this, a)
              }
              throw Error("Invalid arity: " + arguments.length);
            };
            d.i = c;
            d.c = b;
            return d
          }()
        }(function(a) {
          var b = a[1];
          if(1 === b) {
            var c = l;
            a[5] = c;
            a[2] = null;
            a[1] = 2;
            return $
          }
          if(2 === b) {
            var c = P(l), d = Date.now.i ? Date.now.i() : Date.now.call(null), b = ce.i(), e = ce.i(), b = Ub.g(N([xe, b, se, e], 0));
            a[6] = b;
            a[7] = 0;
            a[8] = c;
            a[9] = d;
            a[2] = null;
            a[1] = 4;
            return $
          }
          return 3 === b ? (b = a[2], lg(a, b)) : 4 === b ? (e = a[7], c = a[8], a[1] = v(e < c) ? 6 : 7, $) : 5 === b ? (c = a[10], b = a[2], c = se.call(null, b), a[10] = b, kg(a, 13, k, c)) : 6 === b ? (e = a[7], d = a[11], b = l.c ? l.c(e) : l.call(null, e), a[11] = b, jg(a, b)) : 7 === b ? (b = a[6], a[2] = b, a[1] = 8, $) : 8 === b ? (b = a[2], a[2] = b, a[1] = 5, $) : 9 === b ? (c = a[12], b = a[2], a[12] = b, a[1] = v(b) ? 10 : 11, $) : 10 === b ? (b = a[6], d = a[11], c = a[12], e = xe.call(null, 
          b), d = Rb.a(e, d), b = se.call(null, b), b = Rb.a(b, c), b = Ub.g(N([xe, d, se, b], 0)), a[2] = b, a[1] = 12, $) : 11 === b ? (b = a[6], a[2] = b, a[1] = 12, $) : 12 === b ? (e = a[7], b = a[2], a[6] = b, a[7] = e + 1, a[2] = null, a[1] = 4, $) : 13 === b ? (c = a[10], b = a[2], c = xe.call(null, c), a[13] = b, a[5] = c, a[2] = null, a[1] = 2, $) : null
        })
      }(), b = function() {
        var b = a.i ? a.i() : a.call(null);
        b[4] = m;
        return b
      }();
      return ig(b)
    });
    return k
  }
  a.n = 2;
  a.l = function(a) {
    var d = I(a);
    a = M(a);
    var e = I(a);
    a = J(a);
    return b(d, e, a)
  };
  a.g = b;
  return a
}();
function Eg() {
  var a = document.getElementById("my-canvas"), b = a.getContext("2d");
  return W([b, a.width, a.height])
}
(function() {
  var a = Eg(), b = R.d(a, 0, null), c = R.d(a, 1, null), d = R.d(a, 2, null), e = Dg.g(W([200, 100]), function() {
    return function() {
      return W([0, 10])
    }
  }(a, b, c, d), N([te, 200, we, 50], 0));
  return function(a, b, c, d, e) {
    return function q() {
      return wg.a(e, function(a, b, c, d) {
        return function(a) {
          b.clearRect(0, 0, c, d);
          a: {
            a = H(a);
            for(var e = null, f = 0, h = 0;;) {
              if(h < f) {
                var k = e.K(e, h), l = ye.call(null, k), m = W([255, 255, 255, l]), s = ve.call(null, k), k = R.d(s, 0, null), s = R.d(s, 1, null), k = Math.floor.c ? Math.floor.c(pf(k, c)) : Math.floor.call(null, pf(k, c)), s = Math.floor.c ? Math.floor.c(pf(s, d)) : Math.floor.call(null, pf(s, d)), l = 10 * l, rd = b, Xf = m;
                rd.fillStyle = sf(m);
                rd.strokeStyle = sf(Xf);
                rd.lineWidth = 1;
                tf(b, W([k, s]), l);
                h += 1
              }else {
                if(a = H(a)) {
                  ic(a) ? (f = xb(a), a = yb(a), e = f, f = P(f)) : (k = I(a), f = ye.call(null, k), e = W([255, 255, 255, f]), m = ve.call(null, k), h = R.d(m, 0, null), m = R.d(m, 1, null), h = Math.floor.c ? Math.floor.c(pf(h, c)) : Math.floor.call(null, pf(h, c)), m = Math.floor.c ? Math.floor.c(pf(m, d)) : Math.floor.call(null, pf(m, d)), f *= 10, l = b, k = e, l.fillStyle = sf(e), l.strokeStyle = sf(k), l.lineWidth = 1, tf(b, W([h, m]), f), a = M(a), e = null, f = 0), h = 0
                }else {
                  break a
                }
              }
            }
          }
          return requestAnimationFrame(q)
        }
      }(a, b, c, d, e))
    }
  }(a, b, c, d, e)()
})();
